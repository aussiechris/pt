/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 13/1/2006
* 
* driver.h
* Example of makefiles.
* Citation: Slide 6-16 of PP2A/PT course notes.
****************************************************************************/

/* Header file inclusions. */
#include <stdio.h>
#include <stdlib.h>
#include "io.h"
#include "calc.h"
