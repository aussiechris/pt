/* to see the output from the preprocessor, you can type */
/* yallara>gcc -E sq.c      */
#include <stdio.h>

#define SQ(x)  ((x) * (x))

int main()
{
  printf("Square of %i is %i \n", 5, SQ(5));
  printf("Square of %i is %i \n", 5+5, SQ(5+5));
  return 0;
}


