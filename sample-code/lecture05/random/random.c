/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 12/1/2006
* 
* random.c
* Example of how to use random file access functions.
*****************************************************************************
* Problem:
* 
* Write the function findCourse(char* course, FILE* fpIn, FILE* fpOut);
*
* This function passes parameters representing a course code, an input file, 
* and an output file. The course code and each line of the input file is in
* COSCXXXX format. The course codes in the input file are sorted in ascending
* order. This function prints a message to the output file indicating if the
* chosen course is found. Example: "Course COSC1098 was NOT found."
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void findCourse(char* course, FILE* fpIn, FILE* fpOut);

int main(void)
{
   /* ... */

   findCourse("COSC8888", fpIn, fpOut);
   findCourse("COSC3456", fpIn, fpOut);

   /* ... */

   return EXIT_SUCCESS;
}

void findCourse(char* course, FILE* fpIn, FILE* fpOut)
{
   /* ... */
}
