/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 12/1/2006
* 
* random.c
* Example of how to use random file access functions.
*****************************************************************************
* Problem:
* 
* Write the function findCourse(char* course, FILE* fpIn, FILE* fpOut);
*
* This function passes parameters representing a course code, an input file, 
* and an output file. The course code and each line of the input file is in
* COSCXXXX format. The course codes in the input file are sorted in ascending
* order. This function prints a message to the output file indicating if the
* chosen course is found. Example: "Course COSC1098 was NOT found."
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define COURSE_LEN 8
#define LINE_LEN 80

void findCourse(char* course, FILE* fpIn, FILE* fpOut);

int main(void)
{
   char* inFile = "input.txt";
   char* outFile = "output.txt";
   FILE* fpIn;
   FILE* fpOut;
   
   if ((fpIn = fopen(inFile, "r")) == NULL)
   {
      printf("Error: Could not open file %s.\n", inFile);
      return EXIT_FAILURE;
   }
   
   if ((fpOut = fopen(outFile, "w")) == NULL)
   {
      printf("Error: Could not open file %s.\n", outFile);
      return EXIT_FAILURE;
   }
   
   findCourse("COSC8888", fpIn, fpOut);
   findCourse("COSC3456", fpIn, fpOut);

   fclose(fpIn);
   fclose(fpOut);

   return EXIT_SUCCESS;
}

void findCourse(char* course, FILE* fpIn, FILE* fpOut)
{
   char line[COURSE_LEN + 1];
   char outString[COURSE_LEN + 1];
   int result;
   
   fseek(fpIn, 0, SEEK_SET);
   
   while (fread(line, sizeof(char), COURSE_LEN + 1, fpIn) != 0)
   {
      result = strncmp(line, course, COURSE_LEN);
      
      if (result == 0)
      {
         sprintf(outString, "Course %s was found.\n", course);
         fwrite(outString, sizeof(char), strlen(outString), fpOut);
         return;
      }
      else if (result > 0)
      {
         sprintf(outString, "Course %s was NOT found.\n", course);
         fwrite(outString, sizeof(char), strlen(outString), fpOut);
         return;
      }
      else
      {
         line[COURSE_LEN] = '\0';
         printf("Checked course %s at offset of %ld bytes.\n", 
                line, ftell(fpIn) - (COURSE_LEN + 1));
      }
   }
   
   sprintf(outString, "Course %s was NOT found.\n", course);
   fwrite(outString, sizeof(char), strlen(outString), fpOut);
}
