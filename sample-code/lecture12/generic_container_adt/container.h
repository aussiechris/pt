#ifndef CONTAINER_H
#define CONTAINER_H

#include <stdio.h>

typedef int (*compare_fn)(const void*, const void*);

typedef void (*free_fn)(void*);

typedef void (*print_fn)(const void*);

struct container;

struct container *container_new(compare_fn, free_fn, print_fn);

int container_insert(struct container*, void*);

int container_delete(struct container*, void*);

void container_print(struct container*);

void container_free(struct container*);

size_t container_size(struct container*);

#endif /* CONTAINER_H */
