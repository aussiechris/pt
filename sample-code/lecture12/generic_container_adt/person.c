#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "person.h"

#define NAME_MAX 20

#define AGE_MIN 0

#define AGE_MAX 122

struct person
{
    char name[NAME_MAX + 1];
    int age;
    /* ... */
};

struct person *person_new(const char *name, int age)
{
    struct person *person = malloc(sizeof(struct person));
    assert(person != NULL);

    if (strlen(name) > NAME_MAX) {
        fprintf(stderr, "Error: name length must be <= %d\n", NAME_MAX);

        free(person);

        return NULL;
    }

    if (age < AGE_MIN || age > AGE_MAX) {
        fprintf(stderr, "Error: %d < age < %d\n", AGE_MIN, AGE_MAX);

        free(person);

        return NULL;
    }

    strcpy(person->name, name);

    person->age = age;

    return person;
}

void person_free(void *vptr)
{
    struct person *person = (struct person*)vptr;
    free(person);
}

void person_print(const void *vptr)
{
    const struct person *person = (const struct person*)vptr;

    printf("{ name: \"%s\", age: %u }\n",
           person->name,
           person->age);
}

/*
  Sort person struct first by name then age with the following semantics
  if lhs == rhs return 0
  if lhs < rhs return -1
  if rhs > rhs return 1
 */
int person_cmp(const void *vptr1, const void *vptr2)
{
    const struct person *lhs = (const struct person*)vptr1;
    const struct person *rhs = (const struct person*)vptr2;

    int result = strcmp(lhs->name, rhs->name);

    if (result == 0)
        return lhs->age < rhs->age;

    return result;
}
