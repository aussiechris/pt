#ifndef PERSON_H
#define PERSON_H

struct person;

struct person *person_new(const char*, int);

void person_free(void*);

void person_print(const void*);

int person_cmp(const void*, const void*);

#endif /* PERSON_H */
