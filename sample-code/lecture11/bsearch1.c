/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 30/1/2006
* 
* bsearch1.c
* Example of bsearch() function.
* Citation: Slides 11-9 to 11-10 of PP2A/PT lecture notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int intCompare(const void*, const void*);

#define ASIZE 10

int main(void)
{
   int a[ASIZE] = { 3, 7, 9, 12, 16, 24, 33, 42, 1138, 2001 };
   int target;
   int *pi;

   printf("Please enter search target (ie. an integer between 0 - 2500):");
   if (scanf("%d", &target) == 1)
   {
      if ((pi = bsearch(&target, a, ASIZE, sizeof(int), intCompare)))
      {
         printf("Found %d.\n", *pi);
      }
      else
      {
         printf("No match found.\n");
      }
   }
   else
   {
      fprintf(stderr, "Failed to fetch search target from user.\n");
   }

   return EXIT_SUCCESS;
}

int intCompare(const void* p1, const void* p2)
{
   const int key1 = *(int*)p1;
   const int key2 = *(int*)p2;

   if (key1 < key2)
   {
      return -1;
   }
   else if (key1 > key2)
   {
      return 1;
   }
   else
   {
      return 0;
   }
}
