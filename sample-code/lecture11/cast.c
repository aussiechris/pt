#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char **argv)
{
    char foo[] = "ABC";
    char *ptr;
    int a;

    a = *(int*)foo;

    printf("%d\n", a);

    ptr = (char*)((int*)&a);

    printf("%s\n", ptr);

    return EXIT_SUCCESS;
}
