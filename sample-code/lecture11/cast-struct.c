#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

struct base {
    int id;
};

struct something {
    int id;
    int a, b, c, d, e;
};

struct something_different {
    int id;
    char *name;
    double a, b;
};

int main(int argc, char **argv)
{
    struct something something = { 42, 1, 1, 1, 1, 1 };
    struct something_different something_different = { 24, "Hello", 1.1, 2.2 };

    struct base *base;

    base = (struct base*)&something;

    printf("%d\n", base->id);

    base = (struct base*)&something_different;

    printf("%d\n", base->id);

    return EXIT_SUCCESS;
}
