#include <stdlib.h>
#include <stdio.h>

#include "example1.h"

#define N 5

void example0()
{
    int array[N] = { 1, 2, 3, 4, 5 };
    int a = 9;
    int b = 9;
    int i;

    for (i = 0; i < N + 2; i++)
        printf("%d ", array[i]);

    printf("\n");
}

int main()
{
    example0();

    example1();

    return EXIT_SUCCESS;
}
