/* 
 * This program has deliberate errors.
 * Use gdb to assist you locating them.
 *
 */

/* This program implements a sorted linked list of integers. */

/* Header file inclusions. */
#include <stdio.h>
#include <stdlib.h>

/* Constants. */
#define SUCCESS 1
#define FAILURE 0

/* Structure definitions. */
typedef struct nodeStruct* IntNodePtr;

typedef struct nodeStruct
{
   int value;
   IntNodePtr next;
} IntNode;

typedef struct listStruct
{
   int size;
   IntNodePtr head;
} IntList;

/* Function prototypes. */
void initList(IntList* pil);
int addNode(IntList* pil, int value);
void displayList(IntList* pil);
int deleteNode(IntList* pil, int value);
void emptyList(IntList* pil);

/* main(): Entry point of the program. */
int main()
{
   IntList il;
   int aaa, bbb, ccc;

   printf("Enter integer #1: ");
   scanf("%d", &aaa);
   printf("Enter integer #2: ");
   scanf("%d", &bbb);
   printf("Enter integer #3: ");
   scanf("%d", &ccc);

   printf("You entered these values: '%d' '%d' '%d'\n", aaa, bbb, ccc);

   initList(&il);
   /* This is code block #1 - generates one error. */
   addNode(&il, aaa);
   addNode(&il, bbb);
   addNode(&il, ccc);
   displayList(&il);

   /* This is code block #2 - generates one error. */
   deleteNode(&il, bbb);
   displayList(&il);

   /* This is code block #3 - generates one error. */
   emptyList(&il);
   displayList(&il);

   return EXIT_SUCCESS;
}

/* initList(): Initialises all members of the linked list. */
void initList(IntList* pil)
{
   pil->head = NULL;
   pil->size = 0;
}

/* addNode(): Inserts a node into the sorted linked list. */
int addNode(IntList* pil, int value)
{
   IntNodePtr new, current, previous;

   /* (1) Create memory for a new node. */
   if ((new = malloc(sizeof(IntNode))) == NULL)
   {
      fprintf(stderr, "Error: Memory allocation failed in add().\n");
      return FAILURE;
   }

   /* (2) Initialise helper pointers. */
   current = pil->head;
   previous = NULL;        /* ERROR #1: This line is missing. */

   /* (3) Search where to put the new node. */
   while (current != NULL && current->value < value)
   {
      previous = current;
      current = current->next;
   }

   /* (4) Populate the new node. */
   new->value = value;
   new->next = current;

   /* (5) Process special case when inserting at head of list. */
   if (previous == NULL)
   {
      pil->head = new;
   }
   else
   {
      previous->next = new;
   }

   /* (6) Increment list size. */
   pil->size++;

   /* (7) Operation was successful. */
   return SUCCESS;
}

/* displayList(): Displays the node values on separate lines. */
void displayList(IntList* pil)
{
   IntNodePtr current = pil->head;

   /* (1) Display nodes one at a time. */
   printf("*** BEGIN DISPLAY ***\n");
   while (current != NULL)
   {
      printf("Current node value is '%d'\n", current->value);
      current = current->next;
   }
   printf("***  END DISPLAY  ***\n");
}  

/* deleteNode(): Removes every node in the list. */
int deleteNode(IntList* pil, int value)
{
   IntNodePtr current, previous;

   /* (1) Initialise helper pointers. */
   current = pil->head;
   previous = NULL;

   /* (2) Search for the node. */
   while (current != NULL && current->value != value)
   {
      previous = current;
      current = current->next;
   }

   /* (3) Return failure if not found. */
   if (current == NULL)
   {
      return FAILURE;
   }

   /* (4) Delete the node. */
   if (previous == NULL)
   {
      pil->head = current->next;
   }
   else
   {
      /* ERROR #2: "current->next" changed to "current". */
      previous->next = current->next;
   }
   free(current);

   /* (5) Decrement list size. */
   pil->size--;

   /* (6) Operation was successful. */
   return SUCCESS;
}

/* emptyList(): Removes every node in the list. */
void emptyList(IntList* pil)
{
   IntNodePtr current, next;

   /* (1) Initialise helper pointer. */
   current = pil->head;

   /* (2) Delete nodes one at a time. Avoid memory leaks. */
   while (current != NULL)  /* ERROR #3: Semi-colon added. */
   {
      next = current->next;
      free(current);
      current = next;
   }

   /* (3) Reset head pointer. */
   pil->head = NULL;
}

/* Debugging strategy:

ERROR #1:
A bus error is generated during the addNode() function. This 
hints to us that a bad pointer is in use. The "where" tells us the line
number where the program crashed. The offending line of code interacts 
with the "previous" pointer, so some time is needed to step through the
addNode() function slowly paying particular attention to this pointer.

ERROR #2:
The program runs without a fatal error. The program only correctly deletes
the node if it is the head node. Therefore, the user is drawn to the code
where there is a difference between processing the head node and other
parts of the linked list - this is step (4). This is an opportunity to
print the contents of the linked list before and after the offending
code without messing up the source code with diagnostic statements.

ERROR #3:
At step (2) of the emptyList() function, user will notice that the program
flow does not leave the while loop condition when stepping through the
program. User will examine this line of code closely and should come to
the conclusion that there is an incorrect semi-colon.

QUESTION: What is a core dump?

A "core dump" is a snapshot of the region of main memory being used
by your program at the time it crashed. Typically the crash will have
been caused by a segmentation fault or a bus error.

A segmentation fault arises when your program attempts to access a memory
location outside of the "segment" of memory allocated to your program when 
it began running as a process.

A bus error arises when your program attempts to access an illegal address.
This could be attempting to write to some Read Only Memory, or perhaps 
attempting to access a non-existent location (nb. typically a machine can
address more memory than is physically installed), or perhaps attempting
to access a 32-bit object on a non-even-byte boundary.

*/
