/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 9/1/2006
* 
* matrix.c
* Two-dimensional array example.
* Citation: Adapted from slide 4-16 of PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define ROWS 3
#define COLS 4

int main(void)
{
   int matrix[ROWS][COLS] = {{1,2,3,4},{5,6,7,8},{9,10,11,12}};
   int i, j, sum = 0;
   
   for (i = 0; i < ROWS; i++)
   {
      for (j = 0; j < COLS; j++)
      {
         sum += matrix[i][j];
         /* Alternatives:
         sum += *(matrix[i] + j);
         sum += *(*(matrix + i) + j);
         sum += (*(matrix + i))[j];
         */
      }
   }

   printf("Sum is: %d\n", sum);
   
   return EXIT_SUCCESS;
}
