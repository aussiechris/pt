/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 7/1/2006
* 
* ptr.c
* Pointer arithmetic example.
* Citation: Adapted from slide 4-7 of PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
   float table[4] = {2.24, 4.24, 3.24, -2.1};
   float *start = table, *end = table + 4;
   float sum = 0.0;

   for ( ; start < end; start++)
   {
	   sum += *start;
   }

   printf("The sum is: %.2f\n", sum);
   
   return EXIT_SUCCESS;
}
