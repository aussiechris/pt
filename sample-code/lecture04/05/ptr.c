/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 7/1/2006
* 
* ptr.c
* Pointer arithmetic example.
* Citation: Adapted from slide 4-5 of PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
   float table[4] = {2.24, 4.24, 3.24, -2.1};
   float *fp;
   float sum = 0.0;
   int  j;

   fp = table;
   
   for (j = 0; j < 4; j++, fp++)
   {
	   sum += *fp;
      /* Normally we might just do: sum += table[j]; */
   }

   printf("The sum is: %.2f\n", sum);
   
   return EXIT_SUCCESS;
}
