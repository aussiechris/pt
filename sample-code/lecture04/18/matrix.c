/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 9/1/2006
* 
* matrix.c
* Two-dimensional array example.
* Citation: Adapted from slide 4-18 of PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define ROWS 3
#define COLS 4

int vectorSum1(int mat[]);
int vectorSum2(int *mat);

int main(void)
{
   int matrix[ROWS][COLS] = {{1,2,3,4},{5,6,7,8},{9,10,11,12}};
   int i, sum1 = 0, sum2 = 0;
   
   for (i = 0; i < ROWS; i++)
   {
      sum1 += vectorSum1(matrix[i]);
   }
   printf("Sum is: %d\n", sum1);
   
   for (i = 0; i < ROWS; i++)
   {
      sum2 += vectorSum2(matrix[i]);
   }
   printf("Sum is: %d\n", sum2);
   
   return EXIT_SUCCESS;
}

int vectorSum1(int mat[])
{
   int i, total = 0;
   
   for (i = 0; i < COLS; i++)
   {
      total += mat[i];
   }
   
   return total;
}

int vectorSum2(int *mat)
{
   int i, total = 0;
   
   for (i = 0; i < COLS; i++)
   {
      total += mat[i];
   }
   
   return total;
}
