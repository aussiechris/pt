#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "container.h"

struct list {
    int data;
    struct list *next;
};

struct container {
    struct list *head;
    size_t size;
};

struct container *container_new ()
{
    struct container *new_container;

    new_container = malloc(sizeof(struct container));
    if (new_container == NULL) {
        perror("container_new: malloc");
        exit(EXIT_FAILURE);
    }

    new_container->head = NULL;
    new_container->size = 0;

    return new_container;
}

int container_insert (struct container *container, int data)
{
    struct list *new_node;

    struct list *prev;
    struct list *curr;

    assert(container != NULL);

    new_node = malloc(sizeof(struct list));
    if (new_node == NULL) {
        perror("container_insert: malloc");
        exit(EXIT_FAILURE);
    }

    new_node->data = data;
    new_node->next = NULL;

    /* linked list insertion (week 8 tutorial) */

    prev = NULL;
    curr = container->head;

    while (curr != NULL && curr->data < new_node->data) {
        prev = curr;
        curr = curr->next;
    }

    if (prev == NULL) /* we are still at the head node */
        container->head = new_node;
    else /* in middle somewhere or at end */
        prev->next = new_node;

    new_node->next = curr;

    container->size++;

    return 0;
}

void container_print (struct container *container)
{
    struct list *curr;

    assert(container != NULL);

    curr = container->head;

    while (curr != NULL) {
        printf("%d ", curr->data);
        curr = curr->next;
    }

    printf("\n");
}

void container_free (struct container *container)
{
    struct list *curr;
    struct list *next;

    assert(container != NULL);

    curr = container->head;

    while (curr != NULL) {
        next = curr->next;
        free(curr);
        curr = next;
    }

    free(container);
}

size_t container_size (struct container *container)
{
    assert(container != NULL);

    return container->size;
}
