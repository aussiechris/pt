#include <stdlib.h>
#include <stdio.h>

#include "container.h"
#include "utility.h"

int main ()
{
    struct container *container;

    container = container_new();

    printf("Please enter person details (^D or blank line to exit)\n\n");

    while (1) {
        char name[BUFSIZ];
        int age;
        int ret;

        ret = get_string("Enter name: ", name);
        if (ret == 0)
            break;

        ret = get_integer("Enter age: ", &age);
        if (ret == 0)
            break;

        container_insert(container, name, age);

        printf("Inserted\n\n");
    };

    printf("\n\ncontainer size: %u\n\n", container_size(container));

    container_print(container);

    container_free(container);

    return EXIT_SUCCESS;
}
