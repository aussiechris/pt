#ifndef PERSON_H
#define PERSON_H

#include <stdio.h>

#define NAME_MAX 20

#define AGE_MIN 0

#define AGE_MAX 122

struct person
{
    char name[NAME_MAX + 1];

    int age;

    /* ... */

    struct person *next;
};

struct person *person_new(const char*, int);

void person_free(struct person*);

void person_print(const struct person*, FILE*);

int person_cmp(const struct person*, const struct person*);

#endif /* PERSON_H */
