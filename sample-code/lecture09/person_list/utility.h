#ifndef UTILITY_H
#define UTILITY_H

int get_integer(const char*, int*);

int get_string(const char*, char*);

#endif /* UTILITY_H */
