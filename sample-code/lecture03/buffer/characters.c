/*
 * Copy the input to the output.
 */
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
   int c; /* next character */
   
   while ((c = getchar()) != EOF)
      putchar(c);
   return EXIT_SUCCESS;
} 
