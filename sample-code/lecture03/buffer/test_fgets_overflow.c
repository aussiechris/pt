/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 5/1/2006 
*
* test_scanf.c
* Demonstration of a program that accepts strings with fgets(), but suffers
* from the infamous "buffer overflow" problem.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define MAX_STR_LEN 10

void readRestOfLine();

int main(void)
{
   char string1[MAX_STR_LEN + 1];
   char string2[MAX_STR_LEN + 1];
   
   printf("Enter string #1: "); 
   fgets(string1, MAX_STR_LEN + 1, stdin);
   readRestOfLine(); 
 
   printf("Enter string #2: ");
   fgets(string2, MAX_STR_LEN + 1, stdin);
   
   printf("String #1 entered: '%s'\n", string1);
   printf("String #2 entered: '%s'\n", string2);
   
   return EXIT_SUCCESS;
}


/* Clears left over input from the input buffer. */
void readRestOfLine()
{
   int c;
  
   while ((c = fgetc(stdin)) != '\n' && c != EOF)
      ;

   clearerr(stdin);
}
