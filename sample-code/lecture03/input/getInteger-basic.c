/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* getInteger-basic.c
*
* This program demonstrates a reusable function for accepting integers.
* If you wish to use this code, you must acknowledge it for plagiarism 
* reasons.
*
* Created June 2006.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INPUT_LENGTH 11
#define PROMPT_LENGTH 80
#define TRUE 1
#define FALSE 0
#define SUCCESS 1
#define FAILURE 0
#define TEMP_STRING_LENGTH 11

void readRestOfLine();
int getInteger(int* integer, unsigned length, char* prompt, int min, int max);

int main(void)
{
   int result = 0;
   char prompt[PROMPT_LENGTH + 1];
   int min = 1, max = 100;

   sprintf(prompt, "Enter an integer (%d - %d): ", min, max);

   getInteger(&result, INPUT_LENGTH, prompt, min, max);

   printf("You entered: '%d'\n", result);

   return EXIT_SUCCESS;
}


/****************************************************************************
* getInteger(): An interactive integer input function.
* This function prompts the user for an integer using a custom prompt. A line
* of text is accepted from the user using fgets() and stored in a temporary
* string. When the function detects that the user has entered too much text,
* an error message is given and the user is forced to reenter the input. The
* function also clears the extra text (if any) with the readRestOfLine()
* function.
* When a valid string has been accepted, the unnecessary newline character
* at the end of the string is overwritten. The function then attempts to 
* convert the string into an integer with strtol(). The function checks to
* see if the input is numberic and within range.
* Finally, the temporary integer is copied to the integer variable that is 
* returned to the calling function.
****************************************************************************/
int getInteger(int* integer, unsigned length, char* prompt, int min, int max)
{
   int finished = FALSE;
   char tempString[TEMP_STRING_LENGTH + 2];
   int tempInteger = 0;
   char* endPtr;

   /* Continue to interact with the user until the input is valid. */
   do
   {
      /* Provide a custom prompt. */
      printf("%s", prompt);
      
      /* Accept input. "+2" is for the \n and \0 characters. */
      fgets(tempString, length + 2, stdin);

      /* A string that doesn't have a newline character is too long. */
      if (tempString[strlen(tempString) - 1] != '\n')
      {
         printf("Input was too long.\n");
         readRestOfLine();
      }
      else
      {
         /* Overwrite the \n character with \0. */
         tempString[strlen(tempString) - 1] = '\0';

         /* Convert string to an integer. */
         tempInteger = (int) strtol(tempString, &endPtr, 10);

         /* Validate integer result. */
         if (strcmp(endPtr, "") != 0)
         {
            printf("Input was not numeric.\n");
         }
         else if (tempInteger < min || tempInteger > max)
         {
            printf("Input was not within range %d - %d.\n", min, max);
         }
         else
         {
            finished = TRUE;
         }
      }
   } while (finished == FALSE);

   /* Make the result integer available to calling function. */
   *integer = tempInteger;

   return SUCCESS;
}


/****************************************************************************
* readRestOfLine(): A function for buffer handling.
* This function reads remaining characters in the standard input buffer
* up to the next newline or EOF. Source:
* https://inside.cs.rmit.edu.au/~sdb/teaching/C-Prog/CourseDocuments/
* FrequentlyAskedQuestions/#alternative
****************************************************************************/
void readRestOfLine()
{
   int c;

   /* read until the end of the line or end-of-file */   
   while ((c = fgetc(stdin)) != '\n' && c != EOF)
      ;

   /* clear the error and end-of-file flags */
   clearerr(stdin);
}
