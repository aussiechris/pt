/* A very simple text editor */
/* p.106 of "C: The complete reference" */
#include <stdio.h>

#define MAX 100
#define LEN 80

char text[MAX][LEN];

int main(void)
{
  int t, i, j;
  
  printf("Enter an empty line to quit. \n");

  for(t=0; t<MAX; t++)
  {
    printf("%d: ", t);
    gets(text[t]);
    if (!*text[t]) break; /* quit on blank line */
  }

  for(i=0; i<t; i++)
  {
    for(j=0; text[i][j]; j++) putchar(text[i][j]);
    putchar('\n');
  }
  
  for(i=0; i<t; i++)
    printf("%s\n", text[i]);
  return 0;
} 
