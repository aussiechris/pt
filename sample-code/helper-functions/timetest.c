/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* March 2006
* 
* timetest.c
* This program prints the day of the week for March 1st 2006.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DAYS_IN_WEEK 7

int main(void)
{
   struct tm date;
   struct tm* return_date;
   time_t date_in_secs;
   const char* weekdays[DAYS_IN_WEEK] = {"Sunday", "Monday", "Tuesday",
                        "Wednesday", "Thursday", "Friday", "Saturday"};
   
   /* Initialise the necessary and known date information. */
   date.tm_sec = 0;     /* seconds after the minute [0, 60]  */
   date.tm_min = 0;     /* minutes after the hour [0, 59] */
   date.tm_hour = 0;    /* hour since midnight [0, 23] */
   date.tm_mday = 1;    /* day of the month [1, 31] */
   date.tm_mon = 2;     /* months since January [0, 11] */
   date.tm_year = 106;  /* years since 1900 */
   date.tm_isdst = 1;   /* flag for daylight savings time */
   
   /* As discussed in the mktime() man page, these elements are ignored
    * when constructing a time_t date with mktime(). */
   /* date.tm_wday; */  /* days since Sunday [0, 6] */
   /* date.tm_yday; */  /* days since January 1 [0, 365] */
   
   /* Convert date from "struct tm" format to "time_t" format. */
   date_in_secs = mktime(&date);
   
   /* Return "time_t" date to "struct tm" format. This time, all other fields
    * in the structure will be populated including the day of the week. */
   return_date = localtime(&date_in_secs);
   
   /* Prove that it works. */
   printf("Day of the week for %d/%d/%d is a %s.\n", 
           return_date->tm_mday, return_date->tm_mon + 1, 
           return_date->tm_year, weekdays[return_date->tm_wday]);

   return EXIT_SUCCESS;
}
