/****************************************************************************
* pointer.c
* Experimenting with the pointer variables.
* Citation: Adapted from slide 1-36 of PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
 
int main(void)
{
   int x, y;
   int *ptr; /* pointer declaration */
   x = 2;
   ptr = &x; /* assigning an address */
   y = *ptr + 3;  /* dereferencing */

   printf("x : %d\n", x); 
   printf("y : %d\n", y);

   printf("ptr : %u\n", ptr);
   printf("&x : %u\n", &x);
   printf("&y : %u\n", &y);
   printf("ptr+3: %u\n", ptr+3);
   return EXIT_SUCCESS;
}
