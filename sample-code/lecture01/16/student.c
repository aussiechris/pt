#include <stdio.h>
#include <stdlib.h>

int main(void)
{
   int ass1, ass2, exam;
   float total;

   if (scanf("%d%d%d", &ass1, &ass2, &exam) != 3)
   {
      printf("Unable to find 3 integers on standard input.\n");
      return EXIT_FAILURE;
   }

   total = ass1 * 0.15 + ass2 * 0.25 + exam * 0.6;

   printf("Assignment 1 = %3d\n", ass1);
   printf("Assignment 2 = %3d\n", ass2);
   printf("Exam         = %3d\n", exam);
   printf("Total        = %6.2f\n", total);

   return EXIT_SUCCESS;
}
