#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int factorial_r (int n)
{
    if (n == 0)
        return 1;

    return n * factorial_r(n - 1);
}

int factorial (int n)
{
   int result = 1;
   int i;

   for (i = n; i >= 1; i--)
      result *= i;

   return result;
}

unsigned long factorial_print_r (unsigned long n)
{
    unsigned long result;

    if (n == 0)
        return 1;

    result = n * factorial_print_r(n - 1);

    printf("%ld %ld\n", n, result);

    return result;
}

int main (int argc, char **argv)
{
    if (argc != 2) {
        fprintf(stderr, "usage %s <n>\n", *argv);
        return EXIT_FAILURE;
    }

    printf("factorial_print_r: %ld\n", factorial_print_r(atoi(argv[1])));

    return EXIT_SUCCESS;
}
