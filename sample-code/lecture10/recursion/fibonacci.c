#include <stdio.h>
#include <stdlib.h>

int fibonacci (int n)
{
    if (n == 1)
        return 0;

    if (n == 2)
        return 1;

    return fibonacci(n-1) + fibonacci(n-2);
}

int main (int argc, char **argv)
{
    int i;

    if (argc != 2) {
        fprintf(stderr, "usage %s <n>\n", *argv);
        return EXIT_FAILURE;
    }

    for (i = 1; i <= atoi(argv[1]); i++)
        printf("%d ", fibonacci(i));

    printf("\n");

    return EXIT_SUCCESS;
}
