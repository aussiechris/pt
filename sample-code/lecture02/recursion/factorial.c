/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 4/1/2006
* 
* factorial.c
* Additional example to demonstrate recursion.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int factorialIteration(int num);
int factorialRecursion(int num);

int main(void)
{
   int num = 4;
   printf("Factorial %d is %d.\n", num, factorialIteration(num));
   printf("Factorial %d is %d.\n", num, factorialRecursion(num));

   return EXIT_SUCCESS;
}

int factorialIteration(int num)
{
   int i, result = 1;
   
   for (i = 1; i <= num; i++)
   {
      result *= i;
   }
   
   return result;
}

int factorialRecursion(int num)
{
   if (num == 1)
   {
      return num;
   }
   else
   {
      return (num * factorialRecursion(num - 1));
   }
}
