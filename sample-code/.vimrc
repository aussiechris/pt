syntax enable
set nonumber
set autoindent
set cindent
set tabstop=3
colorscheme darkblue
set hlsearch
set mouse=a
set history=100
set nocompatible
set backspace=2

