/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 23/1/2006
* 
* malloc.c
* Example of malloc function.
* Citation: Adapted from slide 8-9 of PT lecture notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STR_SIZE 60

char* makeString(); 

int main(void)
{
   char* string1 = "I am a fixed memory string";
   char* string2;
   char* string3;
   
   if ((string2 = malloc(STR_SIZE + 1)) == NULL)
   {
      printf("Unable to allocate %d bytes of memory.\n", STR_SIZE + 1);
      return EXIT_FAILURE;
   }
   strcpy(string2, "I am a dynamically allocated string");
  
   string3 = makeString();
 
   printf("%s\n", string1);
   printf("%s\n", string2);
   printf("%s\n", string3);

/*   free(string2);*/
   free(string3);

   return EXIT_SUCCESS;
}

/* Task: Create a function that returns dynamically allocated memory. */
char* makeString()
{
   char* string3;
   
   if ((string3 = malloc(STR_SIZE + 1)) == NULL)
   {
      printf("Unable to allocate %d bytes of memory.\n", STR_SIZE + 1);
      return NULL;
   }
   strcpy(string3, "I am a dynamically allocated string");

   return string3;
}

