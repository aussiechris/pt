#include <stdio.h>
#include <stdlib.h>

int sum (int , int , int );
void swapByValue(int c, int d);
void swapByReference(int *aPtr, int *bPtr);


int main(void)
{
	int a =1, b =2, c=3;
	printf("Sum is %d\n", sum(a,b,c));
	swapByValue(a,b);
	printf("a = %d and b = %d \n",a,b);
	swapByReference(&a,&b);
	printf("a = %d and b = %d \n",a,b);
	return EXIT_SUCCESS;
}

int sum (int a, int b, int c)
{
	int sumValue;
    sumValue = a+b+c;
    return sumValue;
}

void swapByValue(int c, int d)
{
    int temp;
    temp = c;
    c = d;
    d = temp;
    printf("c = %d and d = %d\n", c, d);
}

void swapByReference(int *aPtr, int *bPtr)
{
    int temp;
    temp = *aPtr;
    *aPtr = *bPtr;
    *bPtr = temp;
    printf("*aPtr = %d and *bPtr = %d\n", *aPtr, *bPtr);
}
