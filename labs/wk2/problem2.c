#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define UNDERLINE for (i = 0; i < 5; i++) printf("%s","--------"); \ printf("\n")

int main(void)
{
    int input, currentnum, square, cube, quartic, quintic;
    currentnum=0;
    /* Read a number*/
    printf("Enter a number: ");
    if (scanf("%d", &input)!=1)
    {
        printf("Oh noes! scanf() failed :( ABORT! ABORT!\n");
        return EXIT_FAILURE;
    }


    /* printf ("Number     Square     Cube       Quartic    Quintic\n");*/
    printf("\n   %-8s%-8s%-8s%-8s%-8s\n", "Number", "Square", "Cube", "Quartic", "Quintic");
    printf ("---------------------------------------------\n");
   /* UNDERLINE;*/
    
    while(currentnum<=input)
    {
        square=currentnum*currentnum;
        cube=square*currentnum;
        quartic=cube*currentnum;
        quintic=quartic*currentnum;
        printf ("     %-8d%-8d%-8d%-8d%-8d\n",currentnum, square, cube, quartic, quintic);
        currentnum++;
    }
    printf ("---------------------------------------------\n");
   /*UNDERLINE*/
    
    return EXIT_SUCCESS;
}
