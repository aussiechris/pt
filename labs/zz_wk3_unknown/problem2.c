
/*
 * The following program was supposed to 
 * print the sequence 
 *    yes
 *    yes 
 * instead of
 *    no
 *    yes 
 *
 * Can you explain why it prints the wrong sequence? 
 * if(x=0) is used (assigning x)
 * should be: if(x==0) (comparing x)
 */

#include <stdio.h>
#include <stdlib.h>
int main(void)
{    
   int x;    

   x = 0;    

   if (x == 0)    
   {        
      printf("yes\n");    
   }    
   else     
   {        
      printf("no\n");    
   }    

   x = 1;    
   if (x == 1)    
   {        
      printf("yes\n");    
   }    
   else     
   {        
      printf("no\n");    
   }    

   return EXIT_SUCCESS;
}

