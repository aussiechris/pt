/*
 * Explain the behaviour of the following program. 
 */

#include <stdio.h>
#include <stdlib.h>

/* Define first ASCII character to start at */
#define FIRST ' '

int main(void)
{    
   int i;    
   /* For all ASCII characters from FIRST (see above) until ASCII 128*/
   for (i = FIRST; i < 128; i++)    
   {        
      /* Print the ASCII number of the character (%d) 
       * followed by the character itself (%c) 
       **/
      printf("%d\t%c\n",i,i);    
   }    

   return EXIT_SUCCESS;
}
