/*
 * This program has a common bug. Can you spot it? 
 * there was a ; character after the for statement
 */

#include <stdio.h>
#include <stdlib.h>

int main(void)
{    
   int i;    

   for (i = 1; i < 11; i++)    
   {        
      printf("This message should be printed 10 times\n");    
   }    

   return EXIT_SUCCESS;
}
