// Basic lab template file
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
    //turn a string into an integer
    char *s = "1";
    char *z = "twenty";
    char *end;
    int i;
    
    //print init values
    printf("s = %s\n",s);
    printf("z = %s\n",z);
    
    // strtol() GOOD!
    printf("strtol:\n");
    i = (int) strtol(s,NULL,10);
    printf("s = %i\n",i);
    i = (int) strtol(z,NULL,10);
    if (!*end)
        printf("z = %i\n",i);
    else
        printf("error\n");
   
    //  atoi() BAD!! - can't tell difference between 0 and error
    printf("atoi:\n");
    i = atoi(s);
    printf("s = %i\n",i);
    i = atoi(z);
    printf("z = %i\n",i);
}
