/** Task 4 **/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(void)
{
	int row, spaces, stars;
	row=1;
	while(row<10)
	{
		spaces=0;
		stars=0;
		while(spaces<(10-row))
		{
			printf(" ");
			spaces++;
		}
		while(stars<(2*row)-1)
		{
			printf("*");
			stars++;
		}
				
		printf("\n");
		row++;
	}
	
	while(row>=1)
	{
		spaces=0;
		stars=0;
		while(spaces<(10-row))
		{
			printf(" ");
			spaces++;
		}
		while(stars<(2*row)-1)
		{
			printf("*");
			stars++;
		}
				
		printf("\n");
		row--;
	
	}
	
return EXIT_SUCCESS;
}
