/** Task 5 **/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(void)
{
	int row, spaces;
	row=1;
	while(row<=11)
	{
		if(row%2==0){
			printf(" ");
		}
		spaces=0;
		while(spaces<=10)
		{
			printf(" *");
			spaces++;
		}
		if(row%2==1){
			printf(" ");
		}		
		printf("\n");
		row++;
	}

return EXIT_SUCCESS;
}
