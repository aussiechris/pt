/** computing teh distance of a marathon in kilometers **/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

main(void)
{
	double miles, yards;
	double kilometers;
	yards = 26;
	miles = 385;
	kilometers = 1.609 * ((miles + yards) /1760.0);
	printf("%f miles, %f yards = %f kilometers\n", miles, yards, kilometers);
	return EXIT_SUCCESS;
}

