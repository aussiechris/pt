" * Make the background dark
set background=dark
" * Turn on syntax highlighting
syntax on
" * Load filetype specific indent and plugin files and turn on filetype
"   detection
filetype indent plugin on
" * Turn on auto indenting
set autoindent
" * Turn on smart indenting - don't use with filetype indenting
" set smartindent
" * Set tabs (ts) and indents (sw) to 4 spaces and save as spaces not tabs
set ts=4 sw=4 et si
" * Wrap cursor keys at start/end of rows
set whichwrap+=<,>,[,],h,l
" * Use the backspace key to delete indents and over line breaks
set backspace=indent,eol,start
" * Highlight search matches
set hlsearch
" * Show line numbers
set number
" * Enable the mouse
set mouse=a
" * Always show the status bar
set laststatus=2
" * Turn on top ruler
set ruler
" * Set showmode on
set smd
