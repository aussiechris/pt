/* This program demonstrates the use of strncpy() function */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_TEXT 20

int main(void) 
{
   char text1[MAX_TEXT] = "";   /* Initialisation of the array */
   char text2[MAX_TEXT] = "World";

   printf("\n Original Text1 = (%s) \n Original Text2 = (%s)\n", text1, text2);

   /** The strncpy() function below, copies the first 3 characters of the text2
    * into text1. ie. the first 3 characters of text1 are *overwritten* by the
    * first 3 characters of text2.
    **/
   strncpy(text1, text2, 3);
   printf("\n Text1 after strncpy() = (%s)", text1);
   printf("\n Text2 after strncpy() = (%s)\n\n", text2);

   return EXIT_SUCCESS;
}
