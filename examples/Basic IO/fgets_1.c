/* 
 *  This example program demonstrates the use of fgets
 *  from the standard C library (stdio)
 */

#include <stdio.h>
#include <stdlib.h>

#define LSIZE 1024

int main(void)
{
   char textInput[LSIZE];

   while (fgets(textInput, LSIZE, stdin) != NULL)  /* fetch up to 1024 chars */
   {
         puts(textInput);                          /* display it if found   */
   
   }                                               /* loop back until EOF   */

   return EXIT_SUCCESS;
}

/*
 * Notes:
 *
 * 0. This example program will continue looping until EOF is encountered.
 *    The user can simulate EOF during interactive input by pressing
 *    control-D on a blank line (default under Unix) or control-Z (MS-DOS/Win)
 *
 * 1. fgets() is a safe string input routine, it is safe because it
 *    requires the caller provide details of the size of the string
 *    buffer so that fgets can ensure it does not fetch more characters
 *    than there is space for within the string buffer. fgets allows
 *    room for nul-termination ('\0') of the string buffer.
 * 
 * 2. if the user (via the operating system) provides a line of text
 *    (lines are delimited with a newline ('\n') when the enter or
 *    return key is pressed (assuming interactive input) shorter
 *    than the available space, then the newline will be added
 *    to the string buffer before the nul-termination.
 *
 * 3. some operating systems (eg. MS-DOS/Windows) will use \r\n 
 *    ie. a carriage return and newline combination to represent the
 *    end of a line, whereas as Unix and some other operating systems
 *    will use just a newline.
 * 
 */
