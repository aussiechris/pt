
/*
 *  This example program demonstrates the use of scanf
 *  from the standard C library (stdio)
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) 
{
   int num;  

   printf("\nEnter a Number :");

   /* scanf() accepts the input from the standard input (keyboard) and 
    * converts it to an integer (specified by %d) and stores it into the 
    * variable 'num'. To store it into the variable, scanf() needs to know 
    * 'where' variable resides in the memory and so it expects an address 
    * rather than just a variable name -- and therefore we pass &num, meaning
    * 'address of variable num' and not plain 'num' 
    *
    * Please note that scanf() returns an integer that specifies the number of
    * successful conversions done by the function -- in the case below there is
    * only one conversion %d (to integer) requested and so the check for '1' 
    */ 
   if(scanf("%d", &num) != 1) {
      printf("\nInvalid input -- could not convert into integer\n");
      return EXIT_FAILURE;
   }
    
   /** Another example of using the printf()  function -- here, within the
    * quotes we use '%d' -- this simply means that at the position where %d is 
    * mentioned replace it with the integer 'value' stored in varibale after 
    * the quotes are closed -- in this case it is the value of the variable num
    **/
   printf("\nThe value entered for number is %d\n", num);

   return EXIT_SUCCESS;
}
