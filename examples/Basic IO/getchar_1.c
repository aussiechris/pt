
/* 
 *  This example program demonstrates the use of getchar/putchar
 *  from the standard C library (stdio)
 */

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
   int c;

   c = getchar();		/* getchar returns an int, not a char */

   (void)putchar(c);		/* putchar accepts an int, not a char */

   return EXIT_SUCCESS;
}

/* 
 * Notes:
 *
 * 1. Most operating systems perform buffered input from the console
 *    (ie. terminal or console keyboard) this means the user must press
 *    the enter or return key to signify to the operating system that
 *    input is complete. 
 * 2. A single call to getchar() will fetch only one character, even
 *    though as highlight in note 1. above often the user will have
 *    entered two characters, one for the program, the other the newline
 *    (corresponding to the enter or return key).
 * 3. When an operating sytem buffers input as described in note 1. above
 *    it almost always passes the newline onto the running program. This
 *    means your program will usually have to fetch (and then discard)
 *    the newline as well, before further data is processed.
 */
