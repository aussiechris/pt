
/*
 *  This example program demonstrates the use of printf
 *  from the standard C library (stdio)
 */

#include <stdlib.h>
#include <stdio.h>

#define STR_SIZE 8

int main(void) 
{
   int numI =10;
   float numF =12.10;
   char ch = 'A';
   char arr[STR_SIZE] = "Hello";

   /** printf() is used to display content to the standard output.
    *  In the statement below the message within the 'control string' is
    *  printed as it is. Please note that there is no 'conversion specification'
    *  in the first printf() statement.
    **/
    printf("\n A simple program to demonstrate use of printf");
 
    /** In the statements below the 'control string' contains the 'conversion 
     *  specification'. Conversion specifications begin with a % symbol 
     *  followed by a conversion character. The conversion character corresponds
     *  to the data type of the variable whose values is to be printed.
     **/
    
   printf("\n %%d for int    %d", numI);
   printf("\n %%f for float  %f", numF);
   printf("\n %%c for char   %c", ch);
   printf("\n %%s for string %s\n", arr);

   return EXIT_SUCCESS;
}

