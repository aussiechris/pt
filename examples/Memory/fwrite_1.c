/** Demonstrates the use of fwrite() **/

#include <stdio.h>
#include <stdlib.h>

#define MAX_LINE 256
#define MAX_NAME 30

int main() {
   FILE *fp;
   char fname[MAX_NAME] = "test1.bin"; 
   int x =10;
   int num= 0;

   /** open a file  in 'write' mode-- read fopen_1.c for more details 
    * */
   if( (fp =fopen(fname, "w")) == NULL) {
      printf("\n fopen failed - could not open file : %s\n", fname);
      exit(EXIT_SUCCESS);
   }

   /** fwrite() writes a specified number of bytes (2nd parameter multiplied
    *  by 3rd parameter) from a memory location (first parameter) to file 
    *  (pointed by the last parameter).
    *  Please note that since the byte blocks are wrtitten to the file, the
    *  content may not be in human readable format. To read the content of the 
    *  file one should use fread() --  refer to fread_1.c
    **/  
   num= fwrite(&x, sizeof(int), 1, fp);
   printf("\n Total number of bytes written to the file = %d", num);

   fclose(fp);

   return(EXIT_SUCCESS);
}

