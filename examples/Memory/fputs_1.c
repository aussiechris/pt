#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE 256
#define MAX_INPUT 50
#define MAX_NAME 25

int main() {
   FILE *fp;
   char fname[MAX_NAME] = "out.txt"; 
   char input[MAX_INPUT] ="";

   /** open a file in 'append' mode; ie. any new input that is written into
    *  the file associated with this ('fp') pointer will be added after the 
    *  after the last line.
    * */
   if( (fp =fopen(fname, "a")) == NULL) {
      printf("\n fopen failed - could not open file : %s\n", fname);
      exit(EXIT_SUCCESS);
   }

   /** fgets() will read one line of input from the stdin (read fgets_1.c and 
    *  fgets_2.c for more details).
    **/
   if(fgets(input, MAX_INPUT, stdin) == NULL) {
      printf("\n Input failed -no data accepted");
      return(EXIT_SUCCESS);
   }
  
   /** fputs() is a file output function - it writes the null terminated string
    *  pointed to by 'input' to the file associated with FILE pointer 'fp'
    *  On successful operation, it return a non-negative number else would
    *  return an EOF on unsuccessful attempt.
    **/   

   printf("\n\n Writing to the file %s ...\n", fname);
   if (fputs(input, fp) == EOF) 
      printf("\n Error in writing to the file %s", fname);

   /** fclose() terminates the association of the file pointer with the file.
    **/
   fclose(fp);

   return(EXIT_SUCCESS);
}

