/** Demonstrates the use of fread() -- please read fwrite_1.c before you read
 *  this code.
 **/

#include <stdio.h>
#include <stdlib.h>

#define MAX_LINE 256
#define MAX_NAME 30

int main() {
   FILE *fp;
   char fname[MAX_NAME] = "test1.bin"; 
   int x;
   int num= 0;

   /** open a file  in 'read' mode-- read fopen_1.c for more details 
    * */
   if( (fp =fopen(fname, "r")) == NULL) {
      printf("\n fopen failed - could not open file : %s\n", fname);
      exit(EXIT_SUCCESS);
   }

   /** fread() reads at most 2nd parameter multiplied by 3rd parameter number 
    *  of bytes from the file pointed to by the last parameter and stores it in
    *  the memory location pointed by the first parameter.
    **/

   num= fread(&x, sizeof(int), 1, fp);
   printf("\n The number read from the file = %d\n", x);

   fclose(fp);

   return(EXIT_SUCCESS);
}

