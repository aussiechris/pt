#include <stdio.h>
#include <stdlib.h>

#define MAX_NAME 25

int main() {
   FILE *fp;
   char fname[MAX_NAME] = "test.txt"; 

   /** fopen() opens a file (filename passed as first parameter) in a specific
    *  mode (passed as the second parameter). The file can be opened in a "r",
    *  "w" or "a" mode for reading, writing and appending.
    **/
   fp =fopen(fname, "r");
   
   /** fopen() returns a pointer to the FILE that is associated with the file
    * fname. If the fname file cannot be accessed a NULL pointer is returned.
    * If the fname file does not exist, an attempt to open the file in "r" mode
    * would return a NUL, whereas in case of "w" and "a" modes, it would create
    * a new file with that name.
    **/
   if (fp == NULL) {
      printf("\n fopen failed - could not open file : %s\n", fname);
      exit(EXIT_SUCCESS);
   }
   else {
      printf("\n fopen successfully opened the file %s", fname);
   }

   /** fclose() terminates the association of the file pointer with the file.
    *  It is a good practise to close all the files that are opened
    **/
   fclose(fp);
   printf("\n file %s closed", fname);

   return(EXIT_SUCCESS);
}

