#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE 256
#define MAX_INPUT 50

int main() {
   FILE *fp;
   char fname[MAX_NAME] = "out.txt"; 
   int input ="";

   /** open a file in 'append' mode; ie. any new input that is written into
    *  the file associated with this ('fp') pointer will be added after the 
    *  after the last line.
    * */
   if( (fp =fopen(fname, "a")) == NULL) {
      printf("\n fopen failed - could not open file : %s\n", fname);
      exit(EXIT_SUCCESS);
   }

   /** fgetc() reads a character from the 'stdin' and returns the value of the
    *  character. If an error occurs while reading or when end of file is 
    *  encountered while reading EOF is returned.
    *  The code below reads from the stdin unless EOF is encountered. To 
    *  simulate EOF under UNIX systems one can press <enter> follwed by CTRL+D
    **/
   while((input =fgetc(stdin)) != EOF) {
      /** fputc writes the value of a character 'input' to the file associated 
       *  with the pointer 'fp'.
       *  In the code below, if an error occurs while writing to the file EOF
       *  is returned and error message is printed to the standard output; 
       *  after which the code breaks out of the 'while' loop construct.
       **/
      if( fputc(input, fp) == EOF) {
         printf("\n fputc failed -- cannot write to the file %s", fname);
         break;
      }
   }

   /** fclose() terminates the association of the file pointer with the file.
    **/
   fclose(fp);

   return(EXIT_SUCCESS);
}

