/** Demonstrates the use of calloc() **/

#include <stdio.h>
#include <stdlib.h>

#define EVEN_BASE 2

void AssignEven(int *, int);
void PrintArray(int *, int);

int main() {
   int *arr;  /* to be used as an array */
   int size;

   /** Accept an integer from the user and fill in that many number of even
    *  numbers (starting form BASE_EVEN) in the array pointed to by 'arr' 
    **/
   printf("\n Enter the number of even numbers that you want to store > ");
   if( scanf("%d", &size) != 1) {
      printf("\n Invalid integer type\n");
      return(EXIT_SUCCESS);
   }

   /**Allocate space such that 'size' number of integers can be stored.
    * calloc() takes two parameters, 1st is the the number of elements (in the
    * case below the number of integers) and the 2nd parameter is the size 
    * (number of bytes) of each element. The call to calloc() would allocate
    * contiguous space in memory for the requested number of elements and 
    * initialise each element with zero; it returns the pointer (base address)
    * to the first element of the sequence. 
    **/

   arr = (int *)calloc(size, sizeof(int));

   if(arr == NULL) {
      printf("\n calloc() falied -- could not allocate requested memory space");
      return(EXIT_SUCCESS);
   }

   AssignEven(arr, size);
   PrintArray(arr, size);

   /** What happens to the dynamic memory that was allocated by the call to
    * calloc() at the end of the program ?? -- read free_1.c
    **/

   return(EXIT_SUCCESS);
}

void AssignEven(int *arr, int size) {
   int i, num;

   /** Assigning first 'size' number of even numbers in 'arr' **/ 
   for( i =0, num=EVEN_BASE; i < size; i++) {
      arr[i] = num;
      num+=EVEN_BASE;
   }  
}

void PrintArray(int *arr, int size) {
   int i;

   for( i =0; i < size; i++) {
      printf("\n %d", arr[i]);
   }
   printf("\n");
}


