/** Demonstrates the use of free() -- please read calloc_1.c or malloc_1.c 
 *  before reading this code.
 **/

#include <stdio.h>
#include <stdlib.h>

#define EVEN_BASE 2

void AssignEven(int *, int);
void PrintArray(int *, int);

int main() {
   int *arr;  /* to be used as an array */
   int size;

   printf("\n Enter the number of even numbers that you want to store > ");
   if( scanf("%d", &size) != 1) {
      printf("\n Invalid integer type");
   }
   if( (arr = (int *)malloc(sizeof(int) *size)) == NULL ) {
      printf("\n calloc() falied -- could not allocate requested memory space");
      return(EXIT_SUCCESS);
   }
   AssignEven(arr, size);
   PrintArray(arr, size);
   
   /** The dynamically allocated memory does not get freed and returned to the
    *  operating system automatically on exit. The programmer needs to 
    *  explicitly free the requested additional memory space before quiting the
    *  program.
    **/
   free(arr);

   return(EXIT_SUCCESS);
}

void AssignEven(int *arr, int size) {
   int i, num;

   /** Assigning first 'size' number of even numbers in 'arr' **/ 
   for( i =0, num=EVEN_BASE; i < size; i++) {
      arr[i] = num;
      num+=EVEN_BASE;
   }  
}

void PrintArray(int *arr, int size) {
   int i;

   for( i =0; i < size; i++) {
      printf("\n %d", arr[i]);
   }
   printf("\n");
}


