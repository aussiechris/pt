#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE 256
#define MAX_INPUT 50

int main() {
   FILE *fp;
   char fname[] = "test.txt"; 
   char input[MAX_INPUT] ="";
   int len;
   /** open a file  -- read fopen_1.c for more details 
    * */
   if( (fp =fopen(fname, "w")) == NULL) {
      printf("\n fopen failed - could not open file : %s\n", fname);
      exit(EXIT_SUCCESS);
   }
    printf("\n Enter text [press Enter - empty line- to finish] : >");
   /** fgets() will read the input from the stdin (read fgets_1.c and 
    *  fgets_2.c for more details) until an empty line is entered.
    **/
   while ( (fgets(input, MAX_INPUT, stdin)) != NULL) {
      len= strlen(input);
      if(input[len-1] == '\n') {
         input[len-1] = '\0';
      }

      if( strlen(input) == 0) {
         printf("\n No input entered - terminating input ...");
	 break;
      }

      fputs(input,fp);
      fputs("\n",fp);
      printf("\n Enter text [press Enter - empty line- to finish] : >");
 
   }

   /** fclose() terminates the association of the file pointer with the file.
    *  It is a good practise to close all the files that are opened
    **/
   printf("\n data written to file - Closing the file %s ...", fname);
   fclose(fp);

   return(EXIT_SUCCESS);
}


