/* This program accepts the data from the standard input using fgets() */ 
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_TEXT 30

int main() {
   char text[MAX_TEXT];
   int len;

   /* fgets() accepts a maximum of MAX_TEXT-1 characters from the 
    * standard input (stdin) and stores it into the character array (text).
    * One encountering either a a newline character or end-of-file, the input
    * process is terminated a null character is inserted into the array after
    * the last input. The address of the first character block of the array
    * (text) is returned at the end of the input process. 
    * */
   printf("\n Enter a text of Maximum %d characters : ", MAX_TEXT-1);

    /* Always check the return value of fgets */
   if (fgets(text, MAX_TEXT, stdin) == NULL) {
      printf("\n Input failed -no data accepted");
      return(EXIT_SUCCESS);
   }
      

    /* Read strlen_1.c for explanation on strlen() */
   len = strlen(text); 

   /* Read fgets_pitfalls.txt for the unexpected output */
   printf("\nThe length of the text (%s) entered is %d", text, len); 

   return(EXIT_SUCCESS);
}

