/** Demonstrates the use of malloc() **/

#include <stdio.h>
#include <stdlib.h>

#define EVEN_BASE 2
void AssignEven(int *, int);
void PrintArray(int *, int);

int main() {
   int *arr;  /* to be used as an array */
   int size;

   /** Accept an integer from the user and fill in that many number of even
    *  numbers (starting form BASE_EVEN) in the array pointed to by 'arr' 
    **/
   printf("\n Enter the number of even numbers that you want to store > ");
   if( scanf("%d", &size) != 1) {
      printf("\n Invalid integer type\n");
      return(EXIT_SUCCESS);
   }

   /**Allocate space such that 'size' number of integers can be stored.
    * malloc() takes the number of bytes to be allocated as a parameter and 
    * returns the pointer to the 1st element (or base address) of the block of
    * space that is allocated.
    **/

   arr = (int *)malloc(sizeof(int) * size);

   if(arr == NULL) {
      printf("\n malloc() falied -- could not allocate requested memory space");
      return(EXIT_SUCCESS);
   }

   AssignEven(arr, size);
   PrintArray(arr, size);

   /** What happens to the dynamic memory that was allocated by the call to
    * malloc() at the end of the program ?? -- read free_1.c
    **/

   return(EXIT_SUCCESS);
}

void AssignEven(int *arr, int size) {
   int i, num;

   /** Assigning first 'size' number of even numbers in 'arr' **/ 
   for( i =0, num=EVEN_BASE; i < size; i++) {
      arr[i] = num;
      num+=EVEN_BASE;
   }
}

void PrintArray(int *arr, int size) {
   int i;

   for( i =0; i < size; i++) {
      printf("\n %d", arr[i]);
   }
   printf("\n");
}


      


