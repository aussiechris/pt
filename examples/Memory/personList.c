/** An example of linked list implementation using the dynamic memory
 *  allocation -- read malloc_1.c and free_1.c before reading this code.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NAME_LEN 30

typedef struct person{
  char name[NAME_LEN];
  int age;
  struct person * nextPerson;
}Person;

void addPerson(Person **, char *);
void printList(Person *);
void removeFirstPerson(Person **);
void destroyList(Person **);

int main() {
   Person * pList =NULL;

   addPerson(&pList, "PMCD");
   addPerson(&pList, "DJDS");
   addPerson(&pList, "ZJ");
  
   printf("\nHere is the complete list of persons :\n");
   printList(pList);

   printf("\nHere is the list of persons AFTER removing the first one :\n");
   removeFirstPerson(&pList);

   printList(pList);
   destroyList(&pList);
  
   return(EXIT_SUCCESS);
}

void addPerson(Person **p, char *name)
{
   /* This function creates a new node and adds it to the first position of 
      the list */

   Person *ptemp;

   /* Since ptemp is a pointer you need to allocate it memory before 
      you can use it */

   ptemp = malloc(sizeof(Person));
   if (ptemp == NULL)
   {
      printf("\nUnable to allocate memory.\n");
      exit(EXIT_FAILURE);
   }

   strcpy(ptemp->name, name);

   if (*p == NULL)  /* ie. List is Empty */
   {
      ptemp->nextPerson = NULL;
   }
   else /* ie. List is NOT empty */
   {
     ptemp->nextPerson = *p;
   }

   *p = ptemp;

}

void printList(Person * p)
{

  while (p != NULL)
  {
     printf("%s\n", p->name);
     p = p->nextPerson;
  } 

}

/** free()s the memory block of teh first element and there by de-linking the
 *  the first node from the list, the address of the first pointer is passed
 *  as the parameter.
 **/
void removeFirstPerson(Person ** p)
{
  Person * ptemp;

  if (*p != NULL)
  {
     ptemp = (*p)->nextPerson;
     free(*p);
     *p = ptemp;
  }
}

/** free()s all the nodes for which the memory space was dynamically allocated **/
void destroyList(Person ** p)
{
  Person * ptemp;

  while (*p != NULL)
  {
     ptemp = (*p)->nextPerson;
     free(*p);
     *p = ptemp;
  } 

}

