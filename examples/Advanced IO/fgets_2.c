/* This program accepts the data from the standard input using fgets() */ 
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define MAX_TEXT 5

int main() {
   char text[MAX_TEXT];
   int len;

   printf("\n Enter a text of Maximum %d characters : ", MAX_TEXT-1);

   /* Always check the return value of fgets */
   if(fgets(text, MAX_TEXT, stdin) == NULL) {
      printf("\n Input failed -no data accepted");
      return(EXIT_SUCCESS);
   }
   len = strlen(text); 

   /** Check if the character before null is a newline character; if it is 
    * a newline character , then replace it with a null character. If it
    * is not a newline character then the user must have attempted to enter more
    * the maximum limit (MAX_LIMIT-1) of the character.
    *
    * Read fgets_pitfalls.txt for further explanation.
    * */
   if(text[len-1] == '\n') 
   {
      text[len-1] = '\0';
   }
   else 
   {
      printf("\nExactly OR More than %d characters entered",MAX_TEXT-1); 
      printf("\nAccept only %d characters, excess chars ignored\n", MAX_TEXT-1);
   }
   printf("\nThe length of the text (%s) entered is %d", text, len); 

   return(0);
}

