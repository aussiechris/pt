#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE 256
#define MAX_NAME 25

int main() {
   FILE *fp;
   char fname[MAX_NAME] = "test.txt"; 
   char line[MAX_LINE];

   /** open a file  -- read fopen_1.c for more details 
    * */
   if( (fp =fopen(fname, "r")) == NULL) {
      printf("\n fopen failed - could not open file : %s\n", fname);
      exit(EXIT_SUCCESS);
   }

   /** fscanf() will read the input from the file pointed by 'fp' and convert
    *  it to a string (%s), integer (%d), float (%f) etc depending on the 
    *  conversion specification (passed as second parameter/s ) and store it
    *  in the corresponding memory address passed as the last parameter/s.
    *
    *  In the statement below fscanf() will read each input from the file 
    *  (delimitted by white space) and convert it into a string and store it in
    *  the memory location specified by 'line'
    *
    *  Please note, if the first parameter of fscanf() is replaced by 'stdin'
    *  it will become similar to scanf()
    **/
   while ( (fscanf(fp, "%s", line)) ==1) {
      printf("\n%s\n", line);
   }

   /** fclose() terminates the association of the file pointer with the file.
    *  It is a good practise to close all the files that are opened
    **/
   fclose(fp);

   return(EXIT_SUCCESS);
}


