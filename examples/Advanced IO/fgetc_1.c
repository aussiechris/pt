#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE 256
#define MAX_NAME 25

int main() {
   FILE *fp;
   char fname[MAX_NAME] = "test.txt"; 
   char line[MAX_LINE];

   /** open a file  -- read fopen_1.c for more details 
    * */
   if( (fp =fopen(fname, "r")) == NULL) {
      printf("\n fopen failed - could not open file : %s\n", fname);
      exit(EXIT_SUCCESS);
   }

   /** fgetc() next character from the file asscociated with 'fp' (passed as 
    *  a parameter) and returns the value of the character read. On reaching
    *  the end of file or if an error occurs while reading, EOF is returned.
    *
    *  Below code reads one character at a time from the file and prints it on 
    *  the screen.
    *
    *  Please note that fgetc() with 'stdin' passed as a parameter behaves 
    *  similar to getc().
    **/
   while ( input = fgetc(fp) !=EOF) {
      printf("\n%c", input);
   }

   /** fclose() terminates the association of the file pointer with the file.
    *  It is a good practise to close all the files that are opened
    **/
   fclose(fp);

   return(EXIT_SUCCESS);
}

