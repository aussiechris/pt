/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1284
* Program Code     : BP094
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
#ifndef TM_COINS_H
#define TM_COINS_H
#include "tm.h"

/* specifies the various coin types in the system and their values */
enum coin_types
{
    FIVE_CENTS=5,
    TEN_CENTS=10,
    TWENTY_CENTS=20,
    FIFTY_CENTS=50,
    ONE_DOLLAR=100,
    TWO_DOLLARS=200
};

/* defines a 'coin' in the system. the coinslist is actually an array of 
 * this type
 */
struct coin
{
    enum coin_types denomination;
    unsigned count;
};


/* function prototypes for my custom functions contained in tm_coins.c */
BOOLEAN coins_init(tm_type_ptr tm);
BOOLEAN coins_load(tm_type_ptr tm, char * coinsfile);
BOOLEAN coins_clear(tm_type_ptr tm);

#endif

