/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1284
* Program Code     : BP094
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
#include "tm_stock.h"

/* add functions here to manipulate the stock list */

/* initialise the data structures for stock */
BOOLEAN stock_init(tm_type_ptr tm)
{
    /* Allocate memory for the stock list */
    tm->stock = malloc(sizeof(struct stock_list));
    /* Check that memory was allocated successfully */
    if(tm->stock)
    {
        /* memory allocated - set initial zero values for stock list */
        tm->stock->num_stock_items = 0;
        tm->stock->head_stock = NULL;
        printf("Stock list initialised OK\n");
        return TRUE;
    }
    else
    {
        printf("Could not allocate memory for stock list\n");
        return FALSE;
    }
}

BOOLEAN stock_load(tm_type_ptr tm, char * stockfile)
{
    char line[INPUT_BUFFER];
    FILE *fp;
    struct stock_node *current_node;
    struct stock_node *next_node;
    char ticket_name[TICKET_NAME_LEN+NULL_CHAR_LENGTH];
    
    /* Open stock file and return any errors */
    fp = fopen(stockfile, "r");
    if (fp==NULL)
    {
        printf ("Error opening file %s",stockfile);
        return FALSE;
    }
    
    /* get data from stockfile line-by-line */
    while (fgets(line, INPUT_BUFFER, fp) != NULL)
    {
        /* new stock item found! increment num_stock_items */
        tm->stock->num_stock_items++;
        
        /* get the name of this ticket */
        strcpy(ticket_name, strtok (line,","));
        
        /* check if this is the first node */
        if (tm->stock->head_stock == NULL)
        {
            /* this is the first node, so create new head */
            tm->stock->head_stock = malloc(sizeof(struct stock_node));
            /* check memory was allocated */
            if (tm->stock->head_stock)
            {
                /* memory allocated ok */
                current_node = tm->stock->head_stock;
                /* set next_node as NULL */
                current_node->next_node = NULL;
            }
            else
            {
                /* memory could not be allocated */
                printf ("Error allocating memory");
                return FALSE;
            }
        }
        else
        {
            /* find next sorted (higher value) node */
            current_node = tm->stock->head_stock;
             
            while(current_node->next_node != NULL && strcmp(current_node->next_node->data->ticket_name,ticket_name)<=0)
            {
                current_node = current_node->next_node;
            }
            /* insert new node */
            if (current_node->next_node != NULL)
            {
                /* new note is in the middle of the list - insert it here */
                next_node = current_node->next_node;
                current_node->next_node = malloc(sizeof(struct stock_node));
                /* check memory was allocated */
                if (current_node->next_node)
                {
                    /* memory allocated ok */
                    current_node = current_node->next_node;
                    current_node->next_node = next_node;
                }
                else
                {
                    /* memory could not be allocated */
                    printf ("Error allocating memory");
                    return FALSE;
                }
            }
            else
            {
                /* hit the last node, so create a new node at the end */
                current_node->next_node = malloc(sizeof(struct stock_node));
                 /* check memory was allocated */
                if (current_node->next_node)
                {
                    /* memory allocated ok */           
                    current_node = current_node->next_node;
                    /* set next_node as NULL */
                    current_node->next_node = NULL;
                }
                else
                {
                    /* memory could not be allocated */
                    printf ("Error allocating memory");
                    return FALSE;
                }
            }
        }
        
        /* allocate memory for this ticket's data */
        current_node->data = malloc(sizeof(struct stock_data));
        /* check memory was allocated */
        if (current_node->data)
        {
            /* memory allocated ok - store data in this node */
            strcpy(current_node->data->ticket_name, ticket_name);
            current_node->data->ticket_type = *strtok(NULL,",");
            strcpy(current_node->data->ticket_zone, strtok (NULL,","));
            current_node->data->ticket_price = atoi(strtok (NULL,","));
            current_node->data->stock_level = atoi(strtok (NULL,","));            
        }
        else
        {
            /* memory could not be allocated - generate error */
            printf ("Error allocating memory");
            return FALSE;
        }
    }
    fclose(fp);
    
    /* print out all tickets after load */
    /*
    current_node = tm->stock->head_stock;
    while(current_node->next_node != NULL)
    {
        printf ("Ticket name: %s\n",current_node->data->ticket_name);
        printf ("Ticket type: %c\n",current_node->data->ticket_type);
        printf ("Ticket zone: %s\n",current_node->data->ticket_zone);
        printf ("Ticket price: %i\n",current_node->data->ticket_price);
        printf ("Stock level: %i\n\n",current_node->data->stock_level);
        current_node = current_node->next_node;
    }
    printf ("Ticket name: %s\n",current_node->data->ticket_name);
    printf ("Ticket type: %c\n",current_node->data->ticket_type);
    printf ("Ticket zone: %s\n",current_node->data->ticket_zone);
    printf ("Ticket price: %i\n",current_node->data->ticket_price);
    printf ("Stock level: %i\n\n",current_node->data->stock_level);
    */
    
    printf ("Stock list loaded OK\n");
    return TRUE;
}
