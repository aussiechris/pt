/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1284
* Program Code     : BP094
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
/**************************************************************************
* main() - this is the entry point into your program. You need to follow 
* the steps in the comments provided as these are the steps required to 
* run your program. Please ensure that you use all function prototypes and 
* datatype declarations provided as not doing so will result in a 
* substantial mark penalty as well as missing out on some important learning
* opportunities.
**************************************************************************/
#include "tm.h"

int main(int argc, char** argv)
{
    tm_type tm;
    BOOLEAN quit = FALSE;    
    tm_type_ptr tm_ptr = &tm;
    
    FILE *fp;
    
    /* check command line arguments */
    if(argc!=NUM_ARGS)
    {   
        /* incorrect number of command line arguments supplied */
        printf("Please supply valid input and output files\nFormat: ./tm <stockfile> <coinsfile>\n");
        return EXIT_FAILURE;
    }
    else
    {
        /* Correct number of command line arguments supplied */
        
        /* check stock file exists*/
        fp = fopen(argv[ARG_STOCK_FILE], "r");
        if(fp==NULL)
        {
        printf("Oh Noes! Could not find the stock file: %s\n",argv[ARG_STOCK_FILE]);
        return EXIT_FAILURE;
        }
        fclose(fp);
        
        /* check coins file exists */
        fp = fopen(argv[ARG_COINS_FILE], "r");
        if(fp==NULL)
        {
        printf("Oh Noes! Could not find the coins file: %s\n",argv[ARG_COINS_FILE]);
        return EXIT_FAILURE;
        }
        fclose(fp);
        printf("Stock and coin files found.\n");
        
    }

    /* initialise data structures */
        printf("Initialising system...\n");
    if (system_init(tm_ptr))
    {
        printf("System initialised successfully!\n");
    }
    else
    {
        printf("Oh Noes! There was an error initialising the system!\nExiting...\n");
        return EXIT_FAILURE;
    }
    
    /* load data */
    if(load_data(tm_ptr, argv[ARG_STOCK_FILE], argv[ARG_COINS_FILE]))
    {
        printf("Data loaded successfully!\n");
    }
    else
    {
        printf("Oh Noes! There was an error loading data!\nExiting...\n");
        return EXIT_FAILURE;
    }
    
    /* test that everything has gone in initialisation and loading */

    while(!quit)
    {

        /* display menu */
        printf("\n\
Main Menu:\n\
1) Purchase Ticket\n\
2) Display Tickets\n\
3) Save and Exit\n\
Administrator-Only Menu:\n\
4) Add Ticket\n\
5) Remove Ticket\n\
6) Display Coins\n\
7) Restock Tickets\n\
8) Restock Coins\n\
9) Abort\n\
\n\
Select your option (1-9):");
        /* perform menu choice */
        switch (get_main_menu_input())
        {
            case MENU_PURCHASE_TICKET:
                /* Display menu */
                printf("\nMENU_PURCHASE_TICKET \
                        \n---------------\n");
                /* void purchase_ticket(tm_type_ptr tm); */
                break;
            case MENU_DISPLAY_TICKETS:
                /* Display menu */
                printf("\nMENU_DISPLAY_TICKETS \
                        \n---------------\n");
                /* void display_tickets(tm_type_ptr tm); */
                break;
            case MENU_SAVE_AND_EXIT:
                /* Save data */
                /* void save_data(tm_type_ptr tm, char * stockfile, char * coinsfile); */
                /* Exit */
                printf("\nSaving Data...\n");
                quit = TRUE;
                break;
            case MENU_ADD_TICKET:
                /* Display menu */
                printf("\nMENU_ADD_TICKET \
                        \n---------------\n");
                /* void add_ticket(tm_type_ptr tm); */
                break;
            case MENU_REMOVE_TICKET:
                /* Display menu */
                printf("\nMENU_REMOVE_TICKET \
                        \n---------------\n");
                /* void delete_ticket(tm_type_ptr tm); */
                break;
            case MENU_DISPLAY_COINS:
                /* Display menu */
                printf("\nMENU_DISPLAY_COINS \
                        \n---------------\n");
                /*void display_coins(tm_type_ptr tm); */
                break;
            case MENU_RESTOCK_TICKETS:
                /* Display menu */
                printf("\nMENU_RESTOCK_TICKETS \
                        \n---------------\n");
                /* void restock_tickets(tm_type_ptr tm); */
                break;
            case MENU_RESTOCK_COINS:
                /* Display menu */
                printf("\nMENU_RESTOCK_COINS \
                        \n---------------\n");
                /* void restock_coins(tm_type_ptr tm); */
                break;
            case MENU_ABORT:
                /* Display menu */
                printf("\nAborting Program...\nNo data has been saved\n");
                quit = TRUE; 
                break;
            case MENU_ERROR:
                /* Display menu */
                printf("\nThere was an input error. \
                        \nPlease try again\n");
                break;
            default:
                printf("\nYour option does not exist\
                        \n           OR\
                        \nhas not been implemented yet\
                        \n");
                break;
        }
    }

    /* free memory */

    /* leave program */
    printf("\nGoodbye!\n");
    return EXIT_SUCCESS; 
}

