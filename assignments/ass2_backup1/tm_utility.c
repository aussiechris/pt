/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1284
* Program Code     : BP094
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/#include "tm_utility.h"

/* add any 'helper' functions here that provide generic service to your 
 * application. read_rest_of_line() is provided here as an example of this
 */

/***************************************************************************
* read_rest_of_line() - reads characters from the input buffer until 
* all characters have been cleared. You will need to call this in 
* association with fgets().
***************************************************************************/
void read_rest_of_line(void)
{
    int ch;
    while(ch=getc(stdin), ch!=EOF && ch!='\n');
    clearerr(stdin);
}

/***************************************************************************
* system_init() - allocate memory if necessary and initialise the struct tm
* to safe initial values.
***************************************************************************/
BOOLEAN system_init(tm_type_ptr tm)
{
    if(!stock_init(tm))
    {
    return FALSE;
    }

    if(!coins_init(tm))
    {
    return FALSE;
    }
    return TRUE;

}

/***************************************************************************
* load_data() - load data from both the stock file and the coins file and 
* populate the datastructures keeping in mind data validation and sorting 
* requirements. This function implements the requirement 2 from the 
* assignment 2 specifications.
***************************************************************************/
BOOLEAN load_data(tm_type * tm, char * stockfile, char * coinsfile)
{
    if(!stock_load(tm, stockfile))
    {
    return FALSE;
    }

    if(!coins_load(tm, coinsfile))
    {
    return FALSE;
    }
    return TRUE;
}

/***************************************************************************
* system_free() - free all memory allocated for the tm datastructure
***************************************************************************/
void system_free(tm_type_ptr tm)
{
}
 
/**************************************************************************
* Get and validate user input for main menu
* Stores input in str menuinput
* Clears buffer if the user enters more than 1 character
**************************************************************************/
char get_main_menu_input(void)
{
    char menuinput[MAX_OPTION_INPUT+EXTRA_SPACES];
    /* Get user input and check it isn't empty */
    if(fgets(menuinput, MAX_OPTION_INPUT+EXTRA_SPACES, stdin) == NULL 
            || menuinput[STR_FIRST_CHAR] == NEWLINE_CHAR)
    {
        /* If the user entered nothing (or rubbish) */
        /* Display an error */
        printf("\nYou didn't input anything useful!\
                \nTry again!");
        return MENU_ERROR;
    }
    /* check for buffer overflow (i.e. \n character exists) */
    else if(menuinput[strlen(menuinput)-NULL_CHAR_LENGTH] != NEWLINE_CHAR )
    {
        /* Buffer has overflown! Report error */ 
        printf("\nYou entered too many characters!\
                \nClearing the input buffer\n");
        /* Clear buffer */
        read_rest_of_line();
        return MENU_ERROR;
    }
    else
    {
        /* If the user entered something */
        /* Return the entered option */
         return menuinput[STR_FIRST_CHAR]; 
    }
}
