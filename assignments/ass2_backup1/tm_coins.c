/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1284
* Program Code     : BP094
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
#include "tm_coins.h"

/* add functions here for the manipulation of the coins list (array) */

/* initialise the coins list with default values */
BOOLEAN coins_init(tm_type_ptr tm)
{

    int i;
    
    /* Allocate memory for the coin list */
    tm->coins = malloc(sizeof(struct coin)*NUM_COINS);
    /* test memory was allocated */
    if (!tm->coins)
    {
        /* memory could not be allocated - return error */
        printf("\nMemory could not be allocated for coins\n");
        return FALSE;
    }
    
    /* set coin denominations */
    tm->coins[0].denomination = TWO_DOLLARS;
    tm->coins[1].denomination = ONE_DOLLAR;
    tm->coins[2].denomination = FIFTY_CENTS;
    tm->coins[3].denomination = TWENTY_CENTS;
    tm->coins[4].denomination = TEN_CENTS;
    tm->coins[5].denomination = FIVE_CENTS;
    
    /* initialise each coin count to 0 */
    for(i=0;i<NUM_COINS;i++)
    {
        tm->coins[i].count = 0;
    }
    
    printf ("Coins initialised OK\n");
    return TRUE;
}

BOOLEAN coins_load(tm_type_ptr tm, char * coinsfile)
{
    char line[INPUT_BUFFER];
    FILE *fp;
    int denomination;
    int i;
    
    /* Open stock file and return any errors */
    fp = fopen(coinsfile, "r");
    if (fp==NULL)
    {
        printf ("Error opening file %s",coinsfile);
        return FALSE;
    }
    
    /* get data from coinfile line-by-line */
    while (fgets(line, INPUT_BUFFER, fp) != NULL)
    {
        /* new coin found! - get the name of this coin */
        denomination = atoi(strtok (line,","));
        
        /* find this coin denomination in the list */
        for(i=0; denomination < tm->coins[i].denomination; i++)
        {
            /* empty for loop */
            /* iterates through coin denominations until match found */
        }
        
        /* assign coin count for this denomination */
        tm->coins[i].count =  atoi(strtok (NULL,","));
    }
    fclose(fp);
    
    /* print out stock levels */
    /*
    printf("OK\nPrinting stock levels:\n");
    for(i=0;i<NUM_COINS;i++)
    {
        printf("Coin value: %3i. Stock level: %3i.\n",tm->coins[i].denomination, tm->coins[i].count );
    } 
    */
    printf ("Coin file loaded OK\n");
    
    return TRUE;
}
