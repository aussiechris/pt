/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Yallara Username : EDIT HERE
* Course Code      : EDIT HERE
* Program Code     : EDIT HERE
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#ifndef TM_H
#define TM_H

typedef struct tm * tm_type_ptr;

typedef enum truefalse
{
    FALSE, TRUE
} BOOLEAN;
#define NUM_COINS 6
#define TICKET_NAME_LEN 20
#define TICKET_ZONE_LEN 10
#define DEFAULT_STOCK_LEVEL 10
#define DEFAULT_COINS_COUNT 50
#define NUM_ARGS 3

#include "tm_stock.h"
#include "tm_coins.h"
#include "tm_utility.h"
#include "tm_options.h"

/* global type definitions */
typedef struct stock_list * stock_list_ptr;
typedef struct coins * coin_list_ptr;
typedef struct tm
{
    coin_list_ptr coins;
    stock_list_ptr stock;
} tm_type;


/* global constants for the program */


#endif
