/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Yallara Username : EDIT HERE
* Course Code      : EDIT HERE
* Program Code     : EDIT HERE
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
#include "tm.h"
#ifndef TM_UTILITY_H
#define TM_UTILITY_H

/* function prototypes for functions implemented in tm_utility.c
 * These are generic helper functions that provide support to the rest
 * of your application.
 *
 * Remember that we will assess the level to which you provide some level
 * of functional abstraction here.
 */
void read_rest_of_line(void);
BOOLEAN system_init(tm_type_ptr tm);
BOOLEAN load_data(tm_type_ptr tm, char * stockfile, char * coinsfile);
#endif
