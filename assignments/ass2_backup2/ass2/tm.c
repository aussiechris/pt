/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1284
* Program Code     : BP094
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
/**************************************************************************
* main() - this is the entry point into your program. You need to follow 
* the steps in the comments provided as these are the steps required to 
* run your program. Please ensure that you use all function prototypes and 
* datatype declarations provided as not doing so will result in a 
* substantial mark penalty as well as missing out on some important learning
* op0portunities.
**************************************************************************/
#include "tm.h"

int main(int argc, char** argv)
{
    tm_type tm;
    BOOLEAN quit = FALSE;
    
    /* Check supplied arguements */
    if(!check_args(argc, argv))
    {
        /* arguements invalid or files not found */
        return EXIT_FAILURE;
    }

    /* initialise data structures */
    if (!system_init(&tm))
    {
        return EXIT_FAILURE;
    }
    
    /* load data */
    if(!load_data(&tm, argv[ARG_STOCK_FILE], argv[ARG_COINS_FILE]))
    {
        return EXIT_FAILURE;
    }
    
    while(!quit)
    {
        /* display menu */
        printf("\n\
Main Menu:\n\
1) Purchase Ticket\n\
2) Display Tickets\n\
3) Save and Exit\n\
Administrator-Only Menu:\n\
4) Add Ticket\n\
5) Remove Ticket\n\
6) Display Coins\n\
7) Restock Tickets\n\
8) Restock Coins\n\
9) Abort\n\
\n\
Select your option (1-9):");

        /* perform menu choice */
        ;
        switch (get_main_menu_input())
        {
            case MENU_PURCHASE_TICKET:
                purchase_ticket(&tm);
                break;
            case MENU_DISPLAY_TICKETS:
                display_tickets(&tm);
                break;
            case MENU_SAVE_AND_EXIT:
                save_data(&tm,  argv[ARG_STOCK_FILE], argv[ARG_COINS_FILE]);
                quit = TRUE;
                break;
            case MENU_ADD_TICKET:
                add_ticket(&tm);
                break;
            case MENU_REMOVE_TICKET:
                delete_ticket(&tm);
                break;
            case MENU_DISPLAY_COINS:
                display_coins(&tm);
                break;
            case MENU_RESTOCK_TICKETS:
                restock_tickets(&tm);
                break;
            case MENU_RESTOCK_COINS:
                restock_coins(&tm);
                break;
            case MENU_ABORT:
                /* Display menu */
                printf("\nAborting Program...\nNo data has been saved\n");
                quit = TRUE; 
                break;
            case MENU_ERROR:
                /* Display menu */
                printf("\nThere was an input error. \
                        \nPlease try again\n");
                break;
            default:
                printf("\nYour option does not exist\
                        \n           OR\
                        \nhas not been implemented yet\
                        \n");
        }
    }

    /* free memory */
    system_free(&tm);
    
    /* leave program */
    printf("\nGoodbye!\n");
    return EXIT_SUCCESS; 
}
