/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1284
* Program Code     : BP094
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/#include "tm_utility.h"

/* add any 'helper' functions here that provide generic service to your 
 * application. read_rest_of_line() is provided here as an example of this
 */

/***************************************************************************
* read_rest_of_line() - reads characters from the input buffer until 
* all characters have been cleared. You will need to call this in 
* association with fgets().
***************************************************************************/
void read_rest_of_line(void)
{
    int ch;
    while(ch=getc(stdin), ch!=EOF && ch!='\n');
    clearerr(stdin);
}

/***************************************************************************
* system_init() - allocate memory if necessary and initialise the struct tm
* to safe initial values.
***************************************************************************/
BOOLEAN system_init(tm_type_ptr tm)
{
    if(!stock_init(tm))
    {
        return FALSE;
    }

    if(!coins_init(tm))
    {
        return FALSE;
    }
    return TRUE;

}

/***************************************************************************
* load_data() - load data from both the stock file and the coins file and 
* populate the datastructures keeping in mind data validation and sorting 
* requirements. This function implements the requirement 2 from the 
* assignment 2 specifications.
***************************************************************************/
BOOLEAN load_data(tm_type * tm, char * stockfile, char * coinsfile)
{
    if(!stock_load(tm, stockfile))
    {
        return FALSE;
    }

    if(!coins_load(tm, coinsfile))
    {
        return FALSE;
    }
    return TRUE;
}

/***************************************************************************
* system_free() - free all memory allocated for the tm datastructure
***************************************************************************/
void system_free(tm_type_ptr tm)
{
}
 
/**************************************************************************
* Get and validate user input for main menu
* Returns single char with menu input
* Clears buffer if the user enters more than 1 character
**************************************************************************/
char get_main_menu_input(void)
{
    char menuinput[MAX_OPTION_INPUT+EXTRA_SPACES];
    /* Get user input and check it isn't empty */
    if(fgets(menuinput, MAX_OPTION_INPUT+EXTRA_SPACES, stdin) == NULL 
            || menuinput[STR_FIRST_CHAR] == NEWLINE_CHAR)
    {
        /* If the user entered nothing (or rubbish) */
        /* Display an error */
        printf("\nYou didn't input anything useful!\
                \nTry again!");
        return MENU_ERROR;
    }
    /* check for buffer overflow (i.e. \n character exists) */
    else if(menuinput[strlen(menuinput)-NULL_CHAR_LENGTH] != NEWLINE_CHAR )
    {
        /* Buffer has overflown! Report error */ 
        printf("\nYou entered too many characters!\
                \nClearing the input buffer\n");
        /* Clear buffer */
        read_rest_of_line();
        return MENU_ERROR;
    }
    else
    {
        /* If the user entered something */
        /* return the entered option */
        return menuinput[STR_FIRST_CHAR]; 
    }
}

/**************************************************************************
* Get and validate length of user input
* Returns char* with user input
* Clears buffer if the user enters more than the specified character length
**************************************************************************/
int get_user_input(int input_length, char* user_input)
{
    char temp_user_input[50+EXTRA_SPACES];
    char *stripped_input;
    
    /* Get user input and check it isn't empty */
    if(fgets(temp_user_input, input_length+EXTRA_SPACES, stdin) == NULL 
            || temp_user_input[STR_FIRST_CHAR] == NEWLINE_CHAR
            || temp_user_input[STR_FIRST_CHAR] == NULL_CHAR
            || temp_user_input[STR_FIRST_CHAR] == EOF)
    {
        /* If the user entered nothing - return to menu */
        return RETURN_TO_MENU;
    }
    /* check for buffer overflow (i.e. \n character exists) */
    else if(temp_user_input[strlen(temp_user_input)-NULL_CHAR_LENGTH] != NEWLINE_CHAR )
    {
        /* Buffer has overflown! Report error */ 
        printf("\nYou entered too many characters!\
                \nClearing the input buffer\n");
        /* Clear buffer */
        read_rest_of_line();
        return MENU_ERROR;
        
    }
    /* remove \n char and trailing string contents */
    stripped_input = strtok(temp_user_input,"\n");
    strcpy(user_input,stripped_input);
    return INPUT_SUCCESS;
}

/**************************************************************************
* Check the command line arguments during init. 
**************************************************************************/
BOOLEAN check_args(int argc, char** argv)
{
    FILE *fp;

    /* check command line arguments */
    if(argc!=NUM_ARGS)
    {   
        /* incorrect number of command line arguments supplied */
        printf("Please supply valid input and output files\nFormat: ./tm <stockfile> <coinsfile>\n");
        return FALSE;
    }
    else
    {
        /* Correct number of command line arguments supplied */
        /* check stock file exists*/
        fp = fopen(argv[ARG_STOCK_FILE], "r");
        if(fp==NULL)
        {
            printf("Oh Noes! Could not find the stock file: %s\n",argv[ARG_STOCK_FILE]);
            return FALSE;
        }
        fclose(fp);
        
        /* check coins file exists */
        fp = fopen(argv[ARG_COINS_FILE], "r");
        if(fp==NULL)
        {
            printf("Oh Noes! Could not find the coins file: %s\n",argv[ARG_COINS_FILE]);
            return FALSE;
        }
        fclose(fp); 
        printf("Arguments checked OK\n");
        return TRUE;     
    }
}

int input_ticket_details(char *ticket_name, char *ticket_type, char *ticket_zone)
{
    char user_input[50+EXTRA_SPACES];
    int input_result;
    
    /* get and validate ticket name */
    do
    {
        printf("\nEnter a ticket name (1-40 characters): ");
        input_result = get_user_input(TICKET_NAME_LEN, ticket_name);
        if (input_result == RETURN_TO_MENU)
            return RETURN_TO_MENU;
        /* validate */
    }
    while (input_result != INPUT_SUCCESS);
    
    /* get and validate ticket type */
    do
    {
        printf("\nEnter a ticket type (1 character): ");
        input_result = get_user_input(1, user_input);
        if (input_result == RETURN_TO_MENU)
            return RETURN_TO_MENU;
        *ticket_type = user_input[STR_FIRST_CHAR];
        /* validate */
    }    
    while (input_result != INPUT_SUCCESS);
    
    /* get and validate ticket zone */
    do
    {
        printf("\nEnter a zone (1, 2, or 1+2): ");
        input_result = get_user_input(TICKET_ZONE_LEN, ticket_zone);
        if (input_result == RETURN_TO_MENU)
            return RETURN_TO_MENU;
        /* validate */
    }
    while (input_result != INPUT_SUCCESS);
    
    return INPUT_SUCCESS;
}
