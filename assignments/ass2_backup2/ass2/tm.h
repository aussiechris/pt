/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1284
* Program Code     : BP094
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#ifndef TM_H
#define TM_H

typedef struct tm * tm_type_ptr;

typedef enum truefalse
{
    FALSE, TRUE
} BOOLEAN;
#define NUM_COINS 6
#define TICKET_NAME_LEN 40
#define TICKET_ZONE_LEN 10
#define DEFAULT_STOCK_LEVEL 10
#define DEFAULT_COINS_COUNT 50
#define NUM_ARGS 3

#include "tm_stock.h"
#include "tm_coins.h"
#include "tm_utility.h"
#include "tm_options.h"

/* global type definitions */
typedef struct stock_list * stock_list_ptr;
typedef struct coin * coin_list_ptr;
typedef struct tm
{
    coin_list_ptr coins;
    stock_list_ptr stock;
} tm_type;


/* global constants for the program */



/* Specify the main menu responses */
#define MENU_PURCHASE_TICKET '1'
#define MENU_DISPLAY_TICKETS '2'
#define MENU_SAVE_AND_EXIT '3'
#define MENU_ADD_TICKET '4'
#define MENU_REMOVE_TICKET '5'
#define MENU_DISPLAY_COINS '6'
#define MENU_RESTOCK_TICKETS '7'
#define MENU_RESTOCK_COINS '8'
#define MENU_ABORT '9'
#define MENU_ERROR '0'

/* Specify Input status codes */
#define RETURN_TO_MENU '1'
#define INPUT_ERROR '2'
#define INPUT_SUCCESS '3'

/* Specify max user input lengths */
#define MAX_USER_INPUT 50

/* This is used to compensate for the extra character spaces taken up by
   the '\n' and '\0' when user is asked for input through fgets(). */
#define EXTRA_SPACES 2

/* Define the length and content of special characters */
#define NULL_CHAR_LENGTH 1
#define NEWLINE_CHAR_LENGTH 1
#define NULL_CHAR '\0'
#define NEWLINE_CHAR '\n'
#define SPACE_CHAR ' '

#define TICKET_TYPE_LEN 1

/* Specifies the base value of various number systems */
#define BINARY 2
#define DECIMAL 10

/* Specify the location of important characters within a string */
#define STR_FIRST_CHAR 0
#define STR_SECOND_CHAR 1

/* Specifies common dollar values in cents */
#define CENTS_PER_DOLLAR 100
#define TEN_DOLLARS 1000
#define HUNDRED_DOLLARS 10000

#endif
