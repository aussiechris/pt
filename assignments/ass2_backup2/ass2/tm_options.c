/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1284
* Program Code     : BP094
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
#include "tm_options.h"

/* add functions here for the implementation of each of the options
 * required to fulfil the requirements of the assignment
 */

/**************************************************************************
* purchase_ticket() - get input from the user about the ticket they wish 
* to purchase, retrieve that ticket from the list and adjust levels of the
* stock and the coins upon payment of the ticket. This function implements
* requirement 4 from the assignment 2 specification.
**************************************************************************/
void purchase_ticket(tm_type * tm)
{
    char ticket_name[TICKET_NAME_LEN+EXTRA_SPACES];
    char ticket_type;
    char ticket_zone[TICKET_ZONE_LEN+EXTRA_SPACES];
    char user_input[MAX_USER_INPUT+EXTRA_SPACES];
    int input_result;
    int ticket_price = 0, coins_input = 0;
    struct coin * coin_buffer;
    int i = 0;
    
    /* init coin buffer */
    coin_buffer = malloc(sizeof(struct coin)*NUM_COINS);
    /* set coin denominations  for coin buffer */
    coin_buffer[0].denomination = TWO_DOLLARS;
    coin_buffer[1].denomination = ONE_DOLLAR;
    coin_buffer[2].denomination = FIFTY_CENTS;
    coin_buffer[3].denomination = TWENTY_CENTS;
    coin_buffer[4].denomination = TEN_CENTS;
    coin_buffer[5].denomination = FIVE_CENTS;
    
    /* initialise each coin count to 0 */
    for(i=0;i<NUM_COINS;i++)
    {
        coin_buffer[i].count = 0;
    }
    
    /* Display menu */
    printf("\nPurchase A Ticket \n-----------------");
    
    /* input ticket details */
    input_result = input_ticket_details(ticket_name, &ticket_type, ticket_zone);

    /* return any input errors */
    if (input_result == RETURN_TO_MENU)
    {
        /* return to menu */
        return;
    }
        
    /* check for at least one ticket in stock */ 
    if (get_stock_level(tm, ticket_name, ticket_type, ticket_zone)==0)
    {
        printf("This ticket is not in stock\n");
        return;
    }
    
    /* get ticket price */
    ticket_price = get_price(tm, ticket_name, ticket_type, ticket_zone);   
    printf("\nThe price is $%4.2f.\n", (float)ticket_price/ONE_DOLLAR);
    
    
    /* input coins */
    do
    {
        printf("\nEnter a coin ($%4.2f remaining):", (float)(ticket_price-coins_input)/ONE_DOLLAR);
        /* get coin */
        input_result = get_user_input(MAX_USER_INPUT, user_input);
        
        /* add valid coins to the coin buffer */
        if (add_coin_buffer(tm, atoi(user_input), coin_buffer))
        {
            /* update coin totals */
            coins_input = coins_input + atoi(user_input);
        }
    }
    while (ticket_price > coins_input);
    
    /* check change can be given */
    if (!check_change(tm, ticket_price, coins_input, coin_buffer))
    {
        printf("\nNot enough change. Sale cancelled");
        return;
    }
    
    /* output change */
    printf("\nThanks for purchasing a %s ",ticket_name);
    if (ticket_type == 'F')
    {
        printf("full fare ");
    }
    else
    {
        printf("consession ");
    }
    printf("zone %s ticket.",ticket_zone);
    printf("\nYour change is: $%4.2f", (float)(coins_input-ticket_price)/ONE_DOLLAR);
    return;
}



/**************************************************************************
* display_tickets() - iterate over the stock list and display a list of 
* tickets in the system according to the format specified in the assignment
* 2 specifications. This option implements requirement 5 fro mthe assignment
* specifications.
**************************************************************************/
void display_tickets(tm_type * tm)
{
    /* print out all tickets in the stock list */
    struct stock_node *current_node;
    
    current_node = tm->stock->head_stock;
    
    /* print header */
    printf ("\nTicket     Type          Zone       Price  Qty\n");
    printf ("---------- ------------- ---------- ------ ---\n");
    
    /* print ticket info */
    while(current_node)
    {
        printf ("%-10s ",current_node->data->ticket_name);
        /* substitute character for full ticket type */
        switch(current_node->data->ticket_type)
        {
            case CONSESSION:
                printf ("%-13s ","Consession");
                break;
         
            case FULL_FARE:
                printf ("%-13s ","Full fare");
                break;
                
        }
        printf ("%-11s",current_node->data->ticket_zone);
        /* check for 4 digit length of ticket price (i.e. $10.00 or more */
        if(current_node->data->ticket_price<TEN_DOLLARS)
        {
            /* pad values less that $10.00 with a space */
            printf (" ");
        }
        printf ("$%2.2f ",(float)current_node->data->ticket_price/CENTS_PER_DOLLAR);
        printf ("%3i\n",current_node->data->stock_level);
        
        /* check for last node */
        if (current_node->next_node)
        {   
            /* set next node */
            current_node = current_node->next_node;
        }
        else
        {
            /* break when we get to last node */
            break;
        }
    }    
}

/***************************************************************************
* add_ticket() - Request user input about creating a new ticket type in 
* the system. You need to validate this input and then create and insert 
* a new ticket into the system, sorted by ticket name. this option 
* implements requirement 6 in the assignment 2 specifications.
**************************************************************************/
void add_ticket(tm_type * tm)
{
 
    char ticket_name[TICKET_NAME_LEN+EXTRA_SPACES];
    char ticket_type;
    char ticket_zone[TICKET_ZONE_LEN+EXTRA_SPACES];
    char user_input[50+EXTRA_SPACES];
    int input_result;
 
    
    /* Display menu */
    printf("\nAdd Ticket \n-----------");
    
    /* input ticket details */
    input_result = input_ticket_details(ticket_name, &ticket_type, ticket_zone);

    /* return any input errors */
    if (input_result == RETURN_TO_MENU)
    {
        /* return to menu */
        return;
    }
    
    /* input coins */
    printf("\nPrice (in cents):");
    /* get coin */
    input_result = get_user_input(TICKET_ZONE_LEN+EXTRA_SPACES, user_input);
    
    
    /* add the ticket to the stock */
    if (!insert_ticket(tm, ticket_name, ticket_type, ticket_zone, atoi(user_input), DEFAULT_STOCK_LEVEL))       
    {
        printf ("Error adding ticket");
        return;
    }
}

/**************************************************************************
* delete_ticket() - Request user input from the administrator specifying
* the name, type and zone of the ticket. You will then search the stock
* list to delete this ticket. All three fields entered must match for the
* ticket to be deleted. Please ensure that you free any memory that was 
* dynamically allocated. This function implements requirement 7 from the 
* assignment 2 specifications.
**************************************************************************/
void delete_ticket(tm_type * tm)
{
    printf("\nMENU_DELETE_TICKET \n---------------\n");
}

/**************************************************************************
* display_coins() - display a list of each of the coin types in the system
* and the count of each coin type according to the format specified in the
* assignment 2 specification. This function implements requirement 8 from
* the assignment 2 specification.
**************************************************************************/
void display_coins(tm_type * tm)
{     
    int i;
    
    /* print header */
    printf ("\nCoin  Quantity Value\n");
    printf ("----- -------- -------\n");
        
    /* load coins from file into coinslist */
    for(i=0; i<NUM_COINS; i++)
    {
        printf("%5i ",tm->coins[i].denomination);
        printf("%8i ",tm->coins[i].count);
        /* add spaces to allow for propper formatting */
        if(tm->coins[i].count*tm->coins[i].denomination<TEN_DOLLARS)
        {
            /* pad values less that $10.00 with a space */
            printf (" ");
        }
        if(tm->coins[i].count*tm->coins[i].denomination<HUNDRED_DOLLARS)
        {
            /* pad values less that $100.00 with another space */
            printf (" ");
        }
        printf("$%4.2f\n", (float)((tm->coins[i].count)*(tm->coins[i].denomination))/CENTS_PER_DOLLAR);
    }
}


/**************************************************************************
* restock_tickets() - iterate over the list and set all ticket products
* to the default level specified in tm.h. This function implements the 
* requirement 9 in the assignment 2 specifications.
**************************************************************************/
void restock_tickets(tm_type * tm)
{
    printf("\nMENU_RESTOCK_TICKET \n---------------\n");
}

/**************************************************************************
* restock_coins() - set the total number of coins of each denomination to
* the default number specified in tm.h. This function implements the 
* requirement 10 from the assignment 2 specifications.
**************************************************************************/
void restock_coins(tm_type * tm)
{
    printf("\nMENU_RESTOCK_COINS \n---------------\n");
}

/**************************************************************************
* save_data() - open the stockfile and the coinsfile for writing and 
* save all the data to these files. You need to ensure that you save data
* to these files in the same format as specified for loading this data in 
* so that your program or any alternate but correct implementation could
* load the resultant data files. This function implements requirement 11
* from the assignment 2 specifications.
**************************************************************************/
void save_data(tm_type * tm, char * stockfile, char * coinsfile)
{
    printf("\nMENU_SAVE_DATA \n---------------\n");
}
