/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1284
* Program Code     : BP094
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
#include "tm_coins.h"

/* add functions here for the manipulation of the coins list (array) */

/* initialise the coins list with default values */
BOOLEAN coins_init(tm_type_ptr tm)
{

    int i;
    
    /* Allocate memory for the coin list */
    tm->coins = malloc(sizeof(struct coin)*NUM_COINS);
    /* test memory was allocated */
    if (!tm->coins)
    {
        printf("\nMemory could not be allocated for coins\n");
        return FALSE;
    }
    
    /* set coin denominations */
    tm->coins[0].denomination = TWO_DOLLARS;
    tm->coins[1].denomination = ONE_DOLLAR;
    tm->coins[2].denomination = FIFTY_CENTS;
    tm->coins[3].denomination = TWENTY_CENTS;
    tm->coins[4].denomination = TEN_CENTS;
    tm->coins[5].denomination = FIVE_CENTS;
    
    /* initialise each coin count to 0 */
    for(i=0;i<NUM_COINS;i++)
    {
        tm->coins[i].count = 0;
    }
    
    printf ("Coins initialised OK\n");
    return TRUE;
}

BOOLEAN coins_load(tm_type_ptr tm, char * coinsfile)
{
    char line[INPUT_BUFFER];
    FILE *fp;
    int denomination;
    int i;
    
    /* Open stock file and return any errors */
    fp = fopen(coinsfile, "r");
    if (fp==NULL)
    {
        printf ("Error opening file %s",coinsfile);
        return FALSE;
    }
    
    /* load coins from file into coinslist */
    while (fgets(line, INPUT_BUFFER, fp) != NULL)
    {
        denomination = atoi(strtok (line,","));
        for(i=0; denomination < tm->coins[i].denomination; i++);
        tm->coins[i].count =  atoi(strtok (NULL,","));
    }
    fclose(fp);
    printf ("Coin file loaded OK\n");
    
    return TRUE;
}

BOOLEAN add_coin_buffer(tm_type_ptr tm, int denomination, struct coin * coin_buffer)
{
    int i;
 
    /* find any matching coins */
    for(i=0;i<NUM_COINS;i++)
    {
        if (tm->coins[i].denomination== denomination)
        {
            coin_buffer[i].count++;
            printf("Coin: %i... x %i.",coin_buffer[i].denomination, coin_buffer[i].count);
            
            return TRUE;
        }
    }
    /* no coin found */
    printf("That's not a real coin!");
    return FALSE;
}

BOOLEAN check_change(tm_type_ptr tm, int ticket_price, int coins_input, struct coin * coin_buffer)
{
    int change = (coins_input-ticket_price);
    int i;
    
    printf("\nChange from coin buffer: ");
    /* see if any change can be given from coin_buffer */
    for(i=0;i<NUM_COINS;i++)
    {
        /* give these coins as change if possible */
        while(change!=0
            && coin_buffer[i].denomination<=change
            && coin_buffer[i].count>0)
        {
            printf("%ic ",coin_buffer[i].denomination);
            change -= coin_buffer[i].denomination;
            coin_buffer[i].count--;
        }        
    }
    
    
    printf("\nChange from coin list: ");
    /* give any remaining change from the coin list */
    for(i=0;i<NUM_COINS;i++)
    {
        /* give these coins as change if possible */
        while(change!=0
            && tm->coins[i].denomination<=change
            && tm->coins[i].count>0)
        {
            printf(" %ic ",tm->coins[i].denomination);
            change -= tm->coins[i].denomination;
            tm->coins[i].count--;
        }        
    }
    
    /* return weather change can be given */
    if (change == 0)
        return TRUE;
    else
        return FALSE;
}
