/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Yallara Username : EDIT HERE
* Course Code      : EDIT HERE
* Program Code     : EDIT HERE
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
/**************************************************************************
* main() - this is the entry point into your program. You need to follow 
* the steps in the comments provided as these are the steps required to 
* run your program. Please ensure that you use all function prototypes and 
* datatype declarations provided as not doing so will result in a 
* substantial mark penalty as well as missing out on some important learning
* opportunities.
**************************************************************************/
#include "tm.h"

int main(int argc, char** argv)
{
    tm_type tm;
    BOOLEAN quit = FALSE;
    /* check command line arguments */


    /* initialise data structures */

    /* load data */

    /* test that everything has gone in initialisation and loading */

    while(!quit)
    {

        /* display menu */

        /* perform menu choice */

    }

    /* free memory */

    /* leave program */
}

