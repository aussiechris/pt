/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1284
* Program Code     : BP094
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
#include "tm.h"
#ifndef TM_UTILITY_H
#define TM_UTILITY_H

/* Specify the function of each command line argument */
#define ARG_PROG_NAME 0
#define ARG_STOCK_FILE 1
#define ARG_COINS_FILE 2

/* Specifies the maximum input length a user can enter for the options
   menu. */
#define MAX_OPTION_INPUT 1

/* Specifies the input buffer for reading files */
#define INPUT_BUFFER 255

/* function prototypes for functions implemented in tm_utility.c
 * These are generic helper functions that provide support to the rest
 * of your application.
 *
 * Remember that we will assess the level to which you provide some level
 * of functional abstraction here.
 */
void read_rest_of_line(void);
BOOLEAN system_init(tm_type_ptr tm);
BOOLEAN load_data(tm_type_ptr tm, char * stockfile, char * coinsfile);
void system_free(tm_type_ptr);

/* function prototypes for my custom functions contained in tm_utility.c */
void display_menu(void);
char get_main_menu_input(void);
int get_user_input(int input_length, char* user_input);
int input_ticket_details(char *ticket_name, char *ticket_type, char *ticket_zone);
BOOLEAN check_args(int argc, char** argv);

#endif
