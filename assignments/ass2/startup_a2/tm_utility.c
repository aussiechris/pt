/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Yallara Username : EDIT HERE
* Course Code      : EDIT HERE
* Program Code     : EDIT HERE
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/#include "tm_utility.h"

/* add any 'helper' functions here that provide generic service to your 
 * application. read_rest_of_line() is provided here as an example of this
 */

/***************************************************************************
* read_rest_of_line() - reads characters from the input buffer until 
* all characters have been cleared. You will need to call this in 
* association with fgets().
***************************************************************************/
void read_rest_of_line(void)
{
    int ch;
    while(ch=getc(stdin), ch!=EOF && ch!='\n') 
        ;
    clearerr(stdin);
}

/***************************************************************************
* system_init() - allocate memory if necessary and initialise the struct tm
* to safe initial values.
***************************************************************************/
BOOLEAN system_init(tm_type * tm)
{
    return FALSE;
}

/***************************************************************************
* load_data() - load data from both the stock file and the coins file and 
* populate the datastructures keeping in mind data validation and sorting 
* requirements. This function implements the requirement 2 from the 
* assignment 2 specifications.
***************************************************************************/
BOOLEAN load_data(tm_type * tm, char * stockfile, char * coinsfile)
{
    return FALSE;
}

