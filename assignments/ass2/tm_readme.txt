/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1284
* Program Code     : BP094
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/

If selected, do you grant permission for your assignment
to be released as an anonymous student sample solution?
--------------------------------------------------------
Yes
SS
Known bugs:
-----------



Incomplete functionality:
-------------------------



Assumptions:
------------



Any other notes for the marker:
-------------------------------

