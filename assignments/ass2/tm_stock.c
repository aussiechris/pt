/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #2 - SCSIT Program Management System
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1284
* Program Code     : BP094
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
#include "tm_stock.h"

/* add functions here to manipulate the stock list */

/**************************************************************************
* stock_init() - initialise the structure of the stock list
**************************************************************************/
BOOLEAN stock_init(tm_type_ptr tm)
{
    /* Allocate memory for the stock list */
    tm->stock = malloc(sizeof(struct stock_list));
    /* Check that memory was allocated successfully */
    if(tm->stock)
    {
        /* memory allocated - set initial zero values for stock list */
        tm->stock->num_stock_items = 0;
        tm->stock->head_stock = NULL;
        printf("Stock list initialised OK\n");
        return TRUE;
    }
    else
    {
        printf("Could not allocate memory for stock list\n");
        return FALSE;
    }
}

/**************************************************************************
* stock_load() -  Load stock from file to the stock list
**************************************************************************/
BOOLEAN stock_load(tm_type_ptr tm, char * stockfile)
{
    char line[INPUT_BUFFER];
    FILE *fp;
    char ticket_name[TICKET_NAME_LEN+NULL_CHAR_LENGTH];
    char ticket_type[2];
    char ticket_zone[TICKET_ZONE_LEN+1];
    unsigned int ticket_price;
    unsigned int stock_level;
    
    /* Open stock file and      any errors */
    fp = fopen(stockfile, "r");
    if (fp==NULL)
    {
        printf ("Error opening file %s",stockfile);
        return FALSE;
    }
    
    /* get data from stockfile line-by-line */
    while (fgets(line, INPUT_BUFFER, fp) != NULL)
    {
        /* new stock item found! increment num_stock_items */
        tm->stock->num_stock_items++;
        
        /* get the data for this ticket */
        strcpy(ticket_name, strtok (line,","));
        strcpy(ticket_type, strtok (NULL,","));
        strcpy(ticket_zone, strtok (NULL,","));
        ticket_price = atoi(strtok (NULL,","));
        stock_level = atoi(strtok (NULL,","));
        
        /* add the ticket to the stock */
        if (!insert_ticket(
            tm, 
            ticket_name, 
            ticket_type[STR_FIRST_CHAR], 
            ticket_zone, 
            ticket_price, 
            stock_level))       
        {
            printf ("Error adding ticket");
            return FALSE;
        }

    }
    fclose(fp);
        
    printf ("Stock list loaded OK\n");
    return TRUE;
}

/**************************************************************************
* stock_reset() -  reset stock levels back to default level
**************************************************************************/
void stock_reset(tm_type_ptr tm)
{
    struct stock_node *current_node; 
    
    /* check there is stock to reset */
    if (tm->stock->head_stock != NULL)
        current_node = tm->stock->head_stock;
    else
        return;
 
    /* for each stock item */
    while (TRUE)
    {
        /* reset stock level */
        current_node->data->stock_level = DEFAULT_STOCK_LEVEL;  
        
        /* move to next node */
        if (current_node->next_node != NULL)
        {
            current_node = current_node->next_node;
        }
        else
            break;
    }
}

/**************************************************************************
* insert_ticket() - insert a ticket into the stock list
* Finds the right location in the list - sorting by ticket name
**************************************************************************/
BOOLEAN insert_ticket(
    tm_type_ptr tm, 
    char * ticket_name, 
    char ticket_type, 
    char * ticket_zone, 
    int ticket_price, 
    int stock_level)
{         
 
    struct stock_node *current_node;
    struct stock_node *next_node;

    /* check if this is the first node */
    if (tm->stock->head_stock == NULL)
    {
        /* this is the first node, so create new head */
        tm->stock->head_stock = malloc(sizeof(struct stock_node));
        /* check memory was allocated */
        if (tm->stock->head_stock)
        {
            /* memory allocated ok */
            current_node = tm->stock->head_stock;
            /* set next_node as NULL */
            current_node->next_node = NULL;
        }
        else
        {
            /* memory could not be allocated */
            printf ("Error allocating memory");
            return FALSE;
        }
    }
    else
    {
        /* find next sorted (higher value) node */
        current_node = tm->stock->head_stock;
            
        while(current_node->next_node != NULL && 
            strcmp(current_node->next_node->data->ticket_name,ticket_name)<=0)
        {
            current_node = current_node->next_node;
        }
        /* insert new node */
        if (current_node->next_node != NULL)
        {
            /* new node is in the middle of the list - insert it here */
            next_node = current_node->next_node;
            current_node->next_node = malloc(sizeof(struct stock_node));
            /* check memory was allocated */
            if (current_node->next_node)
            {
                /* memory allocated ok */
                current_node = current_node->next_node;
                current_node->next_node = next_node;
            }
            else
            {
                /* memory could not be allocated */
                printf ("Error allocating memory");
                return FALSE;
            }
        }
        else
        {
            /* hit the last node, so create a new node at the end */
            current_node->next_node = malloc(sizeof(struct stock_node));
            /* check memory was allocated */
            if (current_node->next_node)
            {
                /* memory allocated ok */           
                current_node = current_node->next_node;
                /* set next_node as NULL */
                current_node->next_node = NULL;
            }
            else
            {
                /* memory could not be allocated */
                printf ("Error allocating memory");
                return FALSE;
            }
        }
    }
        
    /* allocate memory for this ticket's data */
    current_node->data = malloc(sizeof(struct stock_data));
    /* check memory was allocated */
    if (current_node->data)
    {
        /* memory allocated ok - store data in this node */
        strcpy(current_node->data->ticket_name, ticket_name);
        current_node->data->ticket_type = ticket_type;
        strcpy(current_node->data->ticket_zone, ticket_zone);
        current_node->data->ticket_price = ticket_price;
        current_node->data->stock_level = stock_level;            
    }
    else
    {
        /* memory could not be allocated - generate error */
        printf ("Error allocating memory");
        return FALSE;
    }
    /* all data saved OK! YAY!!!!!! */
    return TRUE;
}

/**************************************************************************
* delete_a_ticket() - insert a ticket into the stock list
* Finds the right location in the list - sorting by ticket name
**************************************************************************/
BOOLEAN delete_a_ticket(
    tm_type_ptr tm, 
    char * ticket_name, 
    char ticket_type, 
    char * ticket_zone)
{         
    struct stock_node *current_node = tm->stock->head_stock;
    struct stock_node *prev_node = NULL;
    
    /* find a matching ticket */
    while (TRUE)
    {
        /* check for match */
        if (strcmp(current_node->data->ticket_name,ticket_name) == 0
            && current_node->data->ticket_type == ticket_type
            && strcmp(current_node->data->ticket_zone,ticket_zone) == 0)
        {
            /* match found! */
            /* set the new next_node pointer */
            if (prev_node == NULL)
            {
                tm->stock->head_stock=current_node->next_node;
            }
            else
            {
                prev_node->next_node=current_node->next_node;
            }
            /* free the current node */
            free(current_node);
            
            return TRUE;
        }
        
        /* move to next node if this one doesn't match ticket_name */
        if (current_node->next_node != NULL)
        {
            prev_node = current_node;
            current_node = current_node->next_node;
        }
        else
            break;
    }
    
    /* end of list, no matching ticket, return false */ 
    return FALSE;
}

/**************************************************************************
* get_price() - returns the price of a matching ticket
**************************************************************************/
int get_price(
    tm_type_ptr tm, 
    char* ticket_name, 
    char ticket_type, 
    char* ticket_zone)
{
    struct stock_node *current_node = tm->stock->head_stock;
    
    /* find a matching ticket */
    while (TRUE)
    {

        /* check for match */
        if (strcmp(current_node->data->ticket_name,ticket_name) == 0
            && current_node->data->ticket_type == ticket_type
            && strcmp(current_node->data->ticket_zone,ticket_zone) == 0)
        {
            printf("Match!");
            printf("Stored: %s...\nEntered: %s...\n", 
                current_node->data->ticket_name, 
                ticket_name);
            /* match found! */
            return current_node->data->ticket_price;
        }
        
        /* move to next node if this one doesn't match ticket_name */
        if (current_node->next_node != NULL)
            current_node = current_node->next_node;
        else
            break;
    }
    
    /* end of list, no matching ticket, return zero price */ 
    return 0;
}

/**************************************************************************
* get_stock_level() - returns the stock level of a matching ticket
**************************************************************************/
int get_stock_level(
    tm_type_ptr tm, 
    char* ticket_name, 
    char ticket_type, 
    char* ticket_zone)
{
    struct stock_node *current_node = tm->stock->head_stock;
    
    /* find a matching ticket */
    while (TRUE)
    {
        /* check for match */
        if (strcmp(current_node->data->ticket_name,ticket_name) == 0
            && current_node->data->ticket_type == ticket_type
            && strcmp(current_node->data->ticket_zone,ticket_zone) == 0)
        {
            /* match found! */
            return current_node->data->stock_level;
        }
        
        /* move to next node if this one doesn't match ticket_name */
        if (current_node->next_node != NULL)
            current_node = current_node->next_node;
        else
            break;
    }
    
    /* end of list, no matching ticket, return zero stock */ 
    return 0;
}

/**************************************************************************
* remove_a_ticket() - removes a single ticket from the stock level 
* of a specific matching ticket
**************************************************************************/
void remove_a_ticket(
    tm_type_ptr tm, 
    char* ticket_name, 
    char ticket_type, 
    char* ticket_zone)
{
    struct stock_node *current_node = tm->stock->head_stock;
    
    /* find a matching ticket */
    while (TRUE)
    {
        /* check for match */
        if (strcmp(current_node->data->ticket_name,ticket_name) == 0
            && current_node->data->ticket_type == ticket_type
            && strcmp(current_node->data->ticket_zone,ticket_zone) == 0)
        {
            /* match found! */
            current_node->data->stock_level--;
			return;
        }
        
        /* move to next node if this one doesn't match ticket_name */
        if (current_node->next_node != NULL)
            current_node = current_node->next_node;
        else
            break;
    }
}
