/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #1 
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1284
* Program Code     : BP094
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
#include "assign1.h"
/***************************************************************************
* This source file contains important functions to be developed and
* used by various menu options, as indicated. Note that every
* function has as its first parameter the optionsStats array, which
* will be appropriately updated for later reporting of menu option 6.
* You may ignore this parameter and its relevance to each function
* until you develop the sessionSummary() function.
 **************************************************************************/

/**************************************************************************
* perfect_squares() - implements the "Perfect Squares" option (requirement 
* 2) from the main menu. You need to determine whether the number passed
* in is a perfect square and let the user know if it is or not. You also
* need to print the perfect square before and after this value. Finally
* you need to update the option_stats variable appropriately.
**************************************************************************/
void perfect_squares(int * option_stats, unsigned number)
{
    double root;
    int before, after;

    /* update option stats */
    option_stats[STATS_PERFECT_SQUARES]++;

    /* get the square root of number */
    root = sqrt(number);

    /* check that root is a perfect square by checking for a whole number */
    if(!(root-(int)root))
    {
        /* We HAVE found perfect square */
        printf("\n%i is a perfect square.",(int)number);
        /* find the square before this one */
        before = (((int)root-1)*((int)root-1));
    }
    else
    {
        /* We have NOT found perfect square */
        printf("\n%i is not perfect square.",(int)number);
        /* find the square before this number */
        before = ((int)root*(int)root);
    }
    /* find square after this number */
    after= (((int)root+1)*((int)root+1));
    /* output squares before & after */
    /* check for the special case of number 1 */
    if ((int)number == 1)
    {
        printf("\nPerfect square before: None.");
    }
    else
    {
        printf("\nPerfect square before: %i.",before);
    }
    printf("\nPerfect square after: %i.",after);
}

/**************************************************************************
* ascii_to_binary() - implements the "ASCII to Binary Generator" option
* (requirement 3) from the main menu. Essentially, you need to implement
* an algorithm for conversion of integers to binary and apply this to 
* every character in the input array (called 'ascii' here) and print out
* the result. Finally, you need to update the option_stats array 
* appropriately 
**************************************************************************/
void ascii_to_binary(int * option_stats, char * ascii)
{
    int current_bit, value, i, b;
    
    /* update option stats */   
    option_stats[STATS_ASCII_TO_BINARY]++;
    
    printf("\nBinary representation: ");
    /* for each character in the string */
    for (i = 0; ascii[i] != NEWLINE_CHAR; i++)
    {
        /* Calculate the decimal value of the binary msb */
        for (b = ASCII_BITS; b > LOWEST_ASCII_BIT; b--)
        {
            if(b == ASCII_BITS)
            {
                /* Set the decimal value of the binary^1 bit 
                 * on the first loop*/
                current_bit=BINARY;
            }
            else
            {
                /* Multiply current bit by the BINARY base to find 
                 * the value of the next bit */
                current_bit = current_bit * BINARY;
            }
        }

        /* Get the decimal value of the current character */
        value = (int)ascii[i];
        
        /* Starting at the MSB, convert decimal value to binary bit-by-bit*/
        while (current_bit >= LOWEST_ASCII_BIT)
        {
            /* Decide if the current bit should be 1 or 0 */
            if(value >= current_bit)
            {
                printf("1");
                /* Subtract value of this bit from the current value */
                value = value - current_bit;
            }
            else
            {
                printf("0");
            }
            
            /* Move to the next (less significant) bit */
            current_bit = current_bit / BINARY;
        }
        /* Insert a space between each binary representation */ 
        printf(" ");
    }
}

/**************************************************************************
* matching_brackets() - implements the "Matching Brackets" option 
* (requirement 4) from the main menu. You need to implement an algorithm 
* which will parse a string passed in and determine whether the brackets 
* all match, including the type of brackets. You need to account for 
* {} [] () and you also need to allow for the nesting of brackets. 
* You need to tell the user whether the brackets match or not. Finally you
* need to update the option_stats array appropriately.
**************************************************************************/
void matching_brackets(int * option_stats, char * test_string)
{
    int i, bracket_level;
    char brackets[MAX_BRACKET_INPUT];
    BOOLEAN found_bracket;
    
    /* update option stats */   
    option_stats[STATS_MATCHING_BRACKETS]++;
    
    /* initialise the brackets array and int */
    bracket_level = 0;
    brackets[bracket_level] = NULL_CHAR;
    found_bracket = FALSE;

    /* for each character in the array - until a NEWLINE_CHAR is reached */
    for (i = 0; test_string[i] != NEWLINE_CHAR; i++)
    {
        /* check if character is an open bracket */
        if (test_string[i] == '(' 
                || test_string[i] == '[' 
                || test_string[i] == '{')
        {
            /* store this character in the next bracket level 
             * of the bracket array */
            brackets[++bracket_level] = test_string[i];
            found_bracket = TRUE;
        }
        /* check if character is a close bracket */
        else if (test_string[i] == ')' 
                || test_string[i] == ']' 
                || test_string[i] == '}')
        {
            /* is closing bracket the same type as last open bracket? */
            if ((brackets[bracket_level] == '(' 
                        && test_string[i] == ')') 
                    || (brackets[bracket_level] == '[' 
                        && test_string[i] == ']') 
                    || (brackets[bracket_level] == '{' 
                        && test_string[i] == '}'))
            {
                /* MATCH! return to lower bracket level*/
                bracket_level--;
            }
            else
            {
                /* Brackets don't match - return to menu */
                printf("\nThe brackets do not match.\n");
                return;
            }
        }
    }    
    
    /* check for unpaired brackets */
    if (bracket_level != 0)
    {
        printf("\nThe brackets do not match.\n");
    }
    /* if there were brackets, they must now be paired */
    else if (found_bracket == TRUE)
    {
        printf("\nThe brackets match.\n");
    }
    /* otherwise there must be no brackets */
    else
    {
        printf("\nNo brackets found.\n");
    }
    return;
}

/**************************************************************************
* format_text() - implements the "Formatting Text" option (requirement 6)
* from the main menu. You will need to parse the text, adding newlines 
* so that no line is longer than "width" which means you will need to 
* replace the first white space character before this with the newline. You
* then need to insert extra spaces so that lines are spaced as evenly as 
* possible. Finally you want to update the option_stats array appropriately.
* 
**************************************************************************/
void format_text(int * option_stats, unsigned width, char * text)
{
    char * current_char_ptr, * start_of_line_ptr; 
    int char_count, word_count, line_count, space_count, total_spaces;
    /* loop counter vars */
    int i, iloop, s;
    
    /* initialise the start of line pointer and remove any leading spaces*/
    start_of_line_ptr = text;
    while (*start_of_line_ptr == SPACE_CHAR)
    {
        start_of_line_ptr++;
    }
    
    /* initialise the infinite loop pretection counter */
    iloop = 0;

    /* update option stats */   
    option_stats[STATS_FORMATTING_TEXT]++;
    printf("\nFormatted text:\n");

    /* print a width guide */
    /*
    for (i = 1; i <= width; i++)
    {
        if (i %10 == 0)
        {
            printf("|");
        }
        else if (i %5 == 0)
        {
            printf("*");
        }
        else
        {
            printf(".");
        }
    }
    */
    printf("\n");

    /* for whole string */
    do
    {
        /* zero all counters */
        char_count = 0;
        word_count = 0;
        space_count = 0;
        line_count = 0;
        current_char_ptr = start_of_line_ptr;
        
        /* count the chars and words in each row */
        while (char_count <= width
                    && *current_char_ptr != NEWLINE_CHAR 
                    && *current_char_ptr != NULL_CHAR)
        {
            /* check for a word that isn't over the max width*/
            if (char_count > 0 
                    && char_count < width
                    && *current_char_ptr == SPACE_CHAR 
                    && *(current_char_ptr-1) != SPACE_CHAR)
            {
                /* word found, add word to counter */
                word_count++;
                /* add a space character between words */
                if (word_count > 1)
                {
                    char_count++;
                    space_count++;
                }
                /* update the character count for this line*/
                line_count = char_count;
            }
            /* add non-space characters to the char count */
            if (*current_char_ptr != SPACE_CHAR)
            {
               char_count++;
            }
            /* move to next char */
            current_char_ptr++;

            /* check for last word */
            if (*current_char_ptr == NEWLINE_CHAR 
                    || *current_char_ptr == NULL_CHAR)
            {
                /* last word found, add word to counter */
                word_count++;
                /* add a space character between words */
                if (word_count > 1
                        && *(current_char_ptr-1) != SPACE_CHAR)
                {
                    char_count++;
                    space_count++;
                }
                /* update the character count for this line*/
                line_count = char_count;
            }
        }
        
        /* print stats and debugging info */
        /*
        printf("\nwidth = %i\
                \nchar_count = %i\
                \nword_count = %i\
                \nspace_count = %i\
                \nline_count = %i\
                \ncurrent_char_ptr = \"%c\"\
                \nstart_of_line_ptr = \"%c\"\n\n",
                width,
                char_count,
                word_count,
                space_count,
                line_count,
                *current_char_ptr,
                *start_of_line_ptr);
        */

        /* figure out the total number of space chars needed
        * to fill this line to max width */
        total_spaces = (width - line_count + space_count);
        
        /* for all words in this row */
        for(i = 0; i < word_count; i++)
        {
            /* output next word */
            while (*start_of_line_ptr != SPACE_CHAR
                    && *start_of_line_ptr != NEWLINE_CHAR
                    && *start_of_line_ptr != NULL_CHAR)
            {
                printf("%c", *start_of_line_ptr++);
            }
            /* if there are spaces, output them between each word*/
            if (space_count > 0 && i <= space_count)
            {
                /* add evenly distributed number of spaces */
                for (s = 0; s < total_spaces / space_count; s++)
                {
                    printf(" ");
                }
                /* add remainder space if needed */
                if (i < total_spaces % space_count)
                {
                    printf(" ");
                }
            }

            /* skip remaining spaces in source string */
            while (*start_of_line_ptr == SPACE_CHAR)
            {
                start_of_line_ptr++;
            }
        }
        /* start new row */
        printf("\n");

        /* increment infinite loop protection counter */
        iloop++;
    }
    /* check if we are at the end of the string and repeat if appropriate */
    while (*current_char_ptr != NEWLINE_CHAR
            && *current_char_ptr != NULL_CHAR
            && iloop <= MAX_TEXT_LINES);

}
/**************************************************************************
* session_summary() - implements the "Session Summary" option (requirement
* 7) from the main menu. In this option you need to display the number of 
* times that each option has been run and then update the count of how 
* many times this function has been run.
**************************************************************************/
void session_summary(int * option_stats)
{
    int i;
    /* update option stats */   
    option_stats[STATS_SESSION_SUMMARY]++;

    /* output headings */
    printf("\nSession Summary\
            \n---------------\
            \nOption Count\
            \n------ -----");

    /* output stats for each menu option*/
    for (i = 0; i < NUM_OPTION_STATS; i++)
    {
        printf("\n %5i %5i", i+1, option_stats[i]);
    }
}
