/****************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #1 
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1284
* Program Code     : BP094
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/

-----------------------------------------------------------------------------
If selected, do you grant permission for your assignment to be released as an
anonymous student sample solution?
-----------------------------------------------------------------------------
Yes
-----------------------------------------------------------------------------

Known bugs:
-----------
Text can not be formatted correctly if a word exceeds the maximum line length.


Incomplete functionality:
-------------------------
None - All functionality completed


Assumptions:
------------
No significant assumptions. 
All my assumptions were answered on the forum. :D


Any other notes for the marker:
-------------------------------
I have used many definitions in my assign1.h file, but have commented them all
They should all be pretty self explanitory.

I have also left some helpful stat reporting/debugging code in my project. It 
is commented out and is not active. Feel free to skip over it, or uncomment it
if you feel is useful.

Thanks! :D
