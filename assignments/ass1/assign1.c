/**************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #1 
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1284
* Program Code     : BP094
* Start up code provided by Paul Miller and Lin Padgham
***************************************************************************/
#include "assign1.h"

int main(void)
{ 
    int i, option_stats[NUM_OPTION_STATS];
    BOOLEAN exit_program;

    /* initialise option_stats to 0*/
    for (i = 0; i < NUM_OPTION_STATS; i++)
    {
        option_stats[i]=0;
    }

    /* loop for displaying the main menu and running the selected
     * option here */
    do
    {
        /* Reset menuexit variable*/
        exit_program = FALSE;

        /* Display Menu */
        display_menu();
        /* Prompt and run selected option */
        switch (get_main_menu_input())
        {
            case MENU_PERFECT_SQUARES:
                /* Display perfect squares menu */
                printf("\nPerfect Squares\
                        \n---------------\n");
                
                /* Run the perfect squares menu function in utility1.c */
                perfect_squares_menu(option_stats);
                break;
            case MENU_ASCII_TO_BINARY:
                /* display ASCII to Binary Generator menu */
                printf("\nASCII to binary generator\
                        \n-------------------------\
                        \n");
                
                /* Run the ascii to binary menu function in utility1.c */
                ascii_to_binary_menu(option_stats);
                break;
            case MENU_MATCHING_BRACKETS:
                /* display Matching Brackets menu */
                printf("\nMatching Brackets\
                        \n-----------------\
                        \n");
                
                /* Run the matching brackets menu function in utility1.c */
                matching_brackets_menu(option_stats);
                break;
            case MENU_FORMATTING_TEXT:
                /* display Formatting Text menu */
                printf("\nFormatting Text\
                        \n---------------\
                        \n");
                
                /* Run the formatting text menu function in utility1.c */
                format_text_menu(option_stats);
                break;
            case MENU_SESSION_SUMMARY:
                /* Run the session summary function in options1.c */
                session_summary(option_stats);
                break;
            case MENU_EXIT:
                /* Set exit variable */
                exit_program = TRUE;
                break;
            case MENU_ERROR:
                /* Handle any errors (or just repeat the menu) */
                break;
            default:
                printf("\nYour option does not exist\
                        \n           OR\
                        \nhas not been implimented yet\
                        \n");
                break;
        }         
    } while(exit_program == FALSE);
    /* Quit the program */
    printf("\nGoodbye!\n");
    return EXIT_SUCCESS;
}
