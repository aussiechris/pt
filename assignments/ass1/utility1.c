/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 2 2013 Assignment #1 
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1284
* Program Code     : BP094
* Start up code provided by Paul Miller and Padgham
***************************************************************************/
#include "assign1.h"

/**************************************************************************
* read_rest_of_line() - clear the buffer of any leftover input. Call this 
* function whenever you detect leftover input in the buffer.
**************************************************************************/
void read_rest_of_line(void)
{
    int ch;
    /* read characters one at a time from standard input until there are
     * no characters left to read
     */
    while (ch=getc(stdin), ch!=EOF && ch!='\n');
    /* reset the error status of the input FILE pointer */
    clearerr(stdin);
}

/**************************************************************************
* Display the main menu
**************************************************************************/
void display_menu(void)
{
    /* Display Menu */
    printf("\n\
            \nMain Menu\
            \n---------\
            \n1) Perfect Squares\
            \n2) ASCII to Binary Generator\
            \n3) Matching Brackets\
            \n4) Formatting Text\
            \n5) Session Summary\
            \n6) Exit\
            \n\
            \nSelect your option: ");
}

/**************************************************************************
* Get and validate user input for sub menus
* Validates to maximum supplied input_length 
* Stores input in the supplied char * input_string
* Clears buffer if the user enters more than 1 character
**************************************************************************/
int get_user_input(int min_length,
        int input_length, 
        int int_min,
        int int_max, 
        char * input_string)
{
    /* Get input and check for return to menu conditions */
    if (fgets(input_string, input_length+EXTRA_SPACES, stdin) == NULL 
            || input_string[STR_FIRST_CHAR] == NEWLINE_CHAR)
    {
        /* User has opted to return to the menu */
        return RETURN_TO_MENU;
    }
    /* Check for buffer overflow 
     * - catches input lengths longer than expected range */
    else if(input_string[strlen(input_string)-NULL_CHAR_LENGTH] 
            != NEWLINE_CHAR )
    {
        /* Buffer has overflown! Report error */ 
        printf("\nYou entered too many characters!\
                \nClearing the input buffer\
                \nTry again...");
        /* Clear buffer */
        read_rest_of_line();
        return INPUT_ERROR;
    }
    /* Check for too few characters */
    else if(strlen(input_string)-NULL_CHAR_LENGTH < min_length)
    {
        /* The minimum length was not met */
        printf("\nYou need to enter more characters!\
                \nTry again...");
        return INPUT_ERROR;
    }
    /* Check int maximum is specified 
     * If it is, check that input is not > specified maximum */
    else if(int_max != NO_INTEGER_VALUE 
                && strtol(input_string, NULL, DECIMAL) > int_max)
    {
        /* Integer is too big */
        printf("\nThe value you entered was too large or of the wrong type.\
                \nTry again...");
        return INPUT_ERROR;
    }
    /* Check int maximum is specified 
     * If it is, check that input is not > specified maximum */
    else if(int_min != NO_INTEGER_VALUE 
                && strtol(input_string, NULL, DECIMAL) < int_min)
    {
        /* Integer is too small */
        printf("\nThe value you entered was too small or of the wrong type.\
                \nTry again...");
        return INPUT_ERROR;
    }

    else
    {
        return INPUT_SUCCESS;
    }
}

/**************************************************************************
* Get and validate user input for main menu
* Stores input in str menuinput
* Clears buffer if the user enters more than 1 character
**************************************************************************/
char get_main_menu_input(void)
{
    char menuinput[MAX_OPTION_INPUT+EXTRA_SPACES];
    /* Get user input and check it isn't empty */
    if(fgets(menuinput, MAX_OPTION_INPUT+EXTRA_SPACES, stdin) == NULL 
            || menuinput[STR_FIRST_CHAR] == NEWLINE_CHAR)
    {
        /* If the user entered nothing (or rubbish) */
        /* Display an error */
        printf("\nYou didn't input anything useful!\
                \nTry again Bozo!");
        return MENU_ERROR;
    }
    /* check for buffer overflow (i.e. \n character exsists) */
    else if(menuinput[strlen(menuinput)-NULL_CHAR_LENGTH] != NEWLINE_CHAR )
    {
        /* Buffer has overflown! Report error */ 
        printf("\nYou entered too many characters!\
                \nClearing the input buffer\n");
        /* Clear buffer */
        read_rest_of_line();
        return MENU_ERROR;
    }
    else
    {
        /* If the user entered something */
        /* Return the entered option */
         return menuinput[STR_FIRST_CHAR]; 
    }
}

/**************************************************************************
* Perfect squares menu option
**************************************************************************/
void perfect_squares_menu(int * option_stats)
{
    char user_input[MAX_SQUARE_INPUT+EXTRA_SPACES];
    int input_status;
    
 /*               printf("\nEnter an integer \
 *                       (maximal number of chars per line): ");
 */                       
    /* get input and repeat until input is valid */
    do
    {
        /* display prompt */
        printf("\nEnter a positive integer (%i-%i): ", 
                MIN_SQUARE_VALUE, 
                MAX_SQUARE_VALUE);
        
        /* get and validate input */
        input_status = get_user_input(
                MIN_INPUT,
                MAX_SQUARE_INPUT,
                MIN_SQUARE_VALUE,
                MAX_SQUARE_VALUE, 
                user_input);

        /* If input was valid, run the function*/
        if (input_status == INPUT_SUCCESS)
        {
            perfect_squares(option_stats, strtol(user_input, NULL, DECIMAL));
        }
    }
    /* loop while input is invalid */
    while (input_status == INPUT_ERROR);
}

/**************************************************************************
* ASCII to binary menu option
**************************************************************************/
void ascii_to_binary_menu(int * option_stats)
{
    char user_input[MAX_ASCII_INPUT+EXTRA_SPACES];
    int input_status;
    
    /* get input and repeat until input is valid */
    do
    {
        printf("\nEnter a string (%i-%i characters): ", 
                MIN_INPUT, 
                MAX_ASCII_INPUT);
        
        /* get and validate input */
        input_status = get_user_input(
                MIN_INPUT,
                MAX_ASCII_INPUT, 
                NO_INTEGER_VALUE, 
                NO_INTEGER_VALUE, 
                user_input);

        /* If input was valid, run the function*/
        if (input_status == INPUT_SUCCESS)
        {
            ascii_to_binary(option_stats, user_input);
        }
    }
    /* loop while input is invalid */
    while (input_status == INPUT_ERROR);
}


/**************************************************************************
* Matching brackets menu option
**************************************************************************/
void matching_brackets_menu(int * option_stats)
{
    char user_input[MAX_BRACKET_INPUT+EXTRA_SPACES];
    int input_status;
    
    /* get input and repeat until input is valid */
    do
    {
        /* display prompt */
        printf("\nEnter a string (%i-%i characters): ", 
                MIN_INPUT, 
                MAX_BRACKET_INPUT);
        
        /* get and validate input */
        input_status = get_user_input(
                MIN_INPUT,
                MAX_BRACKET_INPUT, 
                NO_INTEGER_VALUE, 
                NO_INTEGER_VALUE, 
                user_input);

        /* If input was valid*/
        if (input_status == INPUT_SUCCESS)
        {
            /* Run function */
            matching_brackets(option_stats, user_input);
        }
    }
    /* loop while input is invalid */
    while (input_status == INPUT_ERROR);
}

/**************************************************************************
* Format text menu option
**************************************************************************/
void format_text_menu(int * option_stats)
{
    char int_input[MAX_TEXT_INPUT+EXTRA_SPACES];
    char user_input[MAX_TEXT_INPUT+EXTRA_SPACES];
    int input_status;
    
    /* get integer input and repeat until input is valid */
    do
    {
        /* display prompt */
        printf("\nEnter an integer (between %i-%i chars per line): ",
                MIN_TEXT_WIDTH_VALUE,
                MAX_TEXT_WIDTH_VALUE); 
        
        /* get and validate input */
        input_status = get_user_input(
                MIN_INPUT,
                MAX_TEXT_WIDTH_INPUT, 
                MIN_TEXT_WIDTH_VALUE,
                MAX_TEXT_WIDTH_VALUE, 
                int_input);

        /* If input was valid*/
        if (input_status == INPUT_SUCCESS)
        {
            /* get string input and repeat until input is valid */
             do
             {
                /* display prompt */
                printf("\nEnter some sentances (between %i-%i chars): ", 
                        MIN_TEXT_INPUT, 
                        MAX_TEXT_INPUT);
        
                /* get and validate input */
                input_status = get_user_input(
                        MIN_TEXT_INPUT,
                        MAX_TEXT_INPUT, 
                        NO_INTEGER_VALUE, 
                        NO_INTEGER_VALUE, 
                        user_input);
    
                /* If input was valid*/
                if (input_status == INPUT_SUCCESS)
                {
                   /* run the function */
                    format_text(option_stats,
                            strtol(int_input, NULL, DECIMAL), 
                            user_input);
                }
            }
        /* loop while string input is invalid */
        while (input_status == INPUT_ERROR);
        }
    }
    /* loop while integer input is invalid */
    while (input_status == INPUT_ERROR);
}
