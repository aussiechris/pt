/***************************************************************************
* COSC1283/1284 - Programming Techniques
* Semester 1 2013 Assignment #1 
* Full Name        : Chris Cody
* Student Number   : 3080693
* Yallara Username : s3080693
* Course Code      : COSC1284
* Program Code     : BP094
* Start up code provided by Paul Miller and Xiaodong Li
***************************************************************************/
#include <stdio.h>
#include <stdlib.h>

/* I added these include lines */
#include <string.h>
#include <math.h>

/* constants go here */
#define NUM_OPTION_STATS 5

/* Specify the location of important characters within a string */
#define STR_FIRST_CHAR 0
#define STR_SECOND_CHAR 1

/* Specify the main menu responses */
#define MENU_PERFECT_SQUARES '1'
#define MENU_ASCII_TO_BINARY '2'
#define MENU_MATCHING_BRACKETS '3'
#define MENU_FORMATTING_TEXT '4'
#define MENU_SESSION_SUMMARY '5'
#define MENU_EXIT '6'
#define MENU_ERROR '0'

/* Specify input function return options */
#define RETURN_TO_MENU 1
#define INPUT_ERROR 2
#define INPUT_SUCCESS 3

/* Specify the stats array value for each menu option */
#define STATS_PERFECT_SQUARES 0
#define STATS_ASCII_TO_BINARY 1
#define STATS_MATCHING_BRACKETS 2
#define STATS_FORMATTING_TEXT 3
#define STATS_SESSION_SUMMARY 4

/* Specifies the base value of various number systems */
#define BINARY 2
#define DECIMAL 10

/* Specifies the number of bits to output for ASCII-Binary conversion */
#define ASCII_BITS 8
#define LOWEST_ASCII_BIT 1

/* This is used to compensate for the extra character spaces taken up by
   the '\n' and '\0' when user is asked for input through fgets(). */
#define EXTRA_SPACES 2

/* Define the length and content of special characters */
#define NULL_CHAR_LENGTH 1
#define NEWLINE_CHAR_LENGTH 1
#define NULL_CHAR '\0'
#define NEWLINE_CHAR '\n'
#define SPACE_CHAR ' '

/* Specifies the maximum input length a user can enter for the options
   menu. */
#define MAX_OPTION_INPUT 1

/* Specifies the maximum and minimum input length for string inputs */
#define MAX_SQUARE_INPUT 7
#define MAX_ASCII_INPUT 5
#define MAX_BRACKET_INPUT 20
#define MIN_TEXT_INPUT 150
#define MAX_TEXT_INPUT 200
#define MAX_TEXT_WIDTH_INPUT 3
#define MIN_INPUT 1

/* Specifies the maximum input value for integer inputs
 * Values not specified in the assignment:
 * MIN_TEXT_WIDTH_VALUE of 10 was chosen because it seems like a logical value
 * MAX_TEXT_WIDTH_VALUE of 80 was chosen because it is the default screen width */
#define MIN_SQUARE_VALUE 1
#define MAX_SQUARE_VALUE 1000000
#define MIN_TEXT_WIDTH_VALUE 10
#define MAX_TEXT_WIDTH_VALUE 80
#define NO_INTEGER_VALUE 0

/* Specify the maximum number of lines to output for text formatting option
 * This is used to prevent words longer than the maximum specified width or
 * other unexpected errors from causing an infinite loop */
#define MAX_TEXT_LINES 15 

/* provides us with a BOOLEAN type for using TRUE and FALSE */
typedef enum true_false
{
    FALSE=0,TRUE
} BOOLEAN;

/* function prototypes for each option to be implemented */
void perfect_squares(int * option_stats, unsigned number);
void ascii_to_binary(int * option_stats, char * ascii);
void matching_brackets(int * option_stats, char * test_string);
void format_text(int * option_stats, unsigned width, char * text);
void session_summary(int * option_stats);

/* function prototypes for additional functions contained in utility1.c */
void read_rest_of_line(void);

/* function prototypes for my custom functions contained in utility1.c */
void display_menu(void);
char get_main_menu_input(void);
int get_user_input(int min_length, 
        int input_lenght, 
        int int_min,
        int int_max, 
        char * input_string);

/* function prototypes for each options menu item in utility1.c */
void perfect_squares_menu(int * option_stats);
void ascii_to_binary_menu(int * option_stats);
void matching_brackets_menu(int * option_stats);
void format_text_menu(int * option_stats);

