#!/bin/sh
for f in *.c *.h ; do
    echo -n "$f "
    awk '
    {
        if (length($0) > max)
        {
            max = length($0);
            maxline = $0;
        }
    } END {
        print max " " maxline
    }' "$f"
done
